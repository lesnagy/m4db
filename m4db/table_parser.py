r"""
A module to parse a collection of tables in 'table format' which looks like

* database_table_name
col val 1, row 1 | col val 2, row 1 | col val 3, row 1
col val 1, row 2 | col val 2, row 2 | col val 3, row 2
col val 1, row 3 | col val 2, row 3 | col val 3, row 3

* another_table_name
col val 1, row 1 | col val 2, row 1 | col val 3, row 1 | col val 4, row 1
col val 1, row 2 | col val 2, row 2 | col val 3, row 2 | col val 4, row 2
col val 1, row 3 | col val 2, row 3 | col val 3, row 3 | col val 4, row 3
col val 1, row 4 | col val 2, row 4 | col val 3, row 4 | col val 4, row 4
col val 1, row 5 | col val 2, row 5 | col val 3, row 5 | col val 4, row 5
"""


def parse_table(filename):
    r"""
    Parse a file with a collection of tables in the format outlined previously

    Args:
        filename: the name of the file containing table data

    Output:
        a 2D array of length M x N, where M is the number of rows and N is the
        number of columns.

    Raises:
        none
        TODO: [Les] should raise exceptions on data validation errors.
    """

    current_table = None

    tables = {}

    with open(filename, 'r') as fin:
        for line in fin:
            line = line.strip()
            if len(line) == 0:
                pass
            elif line[0] == '*':
                current_table = line[1:].strip()
                tables[current_table] = []
            elif current_table is not None:
                columns = line.split('|')
                if len(columns) > 0:
                    row = [col.strip() for col in columns]
                    tables[current_table].append(row)

    return tables



