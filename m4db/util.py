r"""
This module contains a set of generally useful utility functions for m4db.
"""

import os
import shutil
import re

import uuid

from m4db.global_config import get_global_variables


def uuid_path(uuid, root=None):
    r"""
    Generates a uuid tree from the input uuid, e.g. if a uuid was like
       * 0cbc8a0d-3b97-4f4d-80e2-293b04c70d93
    then it should result in a directory tree that looks like
       * <root>/0c/bc/8a/.../93/
    or if no root has been supplied
       * 0c/bc/8a/.../93/


    :param uuid: a universal unique identifier
    :param root: a root prefix directory (or None if there is no prefix)

    :return: a python path object
    """

    str_uid = str(uuid)
    uid = str_uid.replace('-', '')

    if root is not None:
        return os.path.join(root, *[uid[i:i+2] for i in range(0, len(uid), 2)])
    else:
        return os.path.join(*[uid[i:i+2] for i in range(0, len(uid), 2)])


def mk_uuid_path(uuid, root=None):
    r"""
    Cretes a uuid tree from the unput uuid, e.g. if a uuid was like
        * 0cbc8a0d-3b97-4f4d-80e2-293b04c70d93
    then it should create a directory tree that looks like
        * <root>/0c/bc/8a/.../93/
    or if no root has been supplied
        * 0c/bc/8a/.../93/

    :param uuid: a universal unique identifier
    :param root: a root prefix directory (or None if there is no prefix)

    :return: a python path object
    """

    dir_path = uuid_path(uuid, root)
    print(dir_path)
    os.makedirs(dir_path, exist_ok=True)

    return dir_path


def clear_directory(directory):
    r"""
    Deletes the contents of a directory.
    :param directory: the name of the directory to be cleared
    :return: None
    """
    names = os.listdir(directory)
    for name in names:
        abs_name = os.path.join(
            directory, name
        )
        if os.path.isfile(abs_name):
            os.remove(abs_name)
        elif os.path.isdir(abs_name):
            shutil.rmtree(abs_name)


def rm_uuid_path(uuidp, root=None, is_path=False):
    r"""
    Deletes a uuid tree.

    :param uuidp:    a uuid or path
    :param root:     a root prefix directory (or None if there is no prefix)
    :param is_path : True if 'uuidp' should be interpreted as a
                     path, otherwise False.
    """
    gcfg = get_global_variables()

    regex_hex = re.compile(gcfg.rstr_uuid)

    uuid_len = 16
    dir_path = None
    if not is_path:
        dir_path = uuid_path(uuidp, root)
    else:
        dir_path = os.path.join(uuidp)


    path_head = dir_path
    for i in range(uuid_len):
        splt = os.path.split(path_head)
        match_hex = regex_hex.match(splt[1])
        if match_hex:
            # The last component was a hex and so we remove the path
            shutil.rmtree(path_head)

            path_head = splt[0]
        else:
            # The last component was not hex, so break the loop
            break



def uuid_path_is_empty(uuidp, root=None, is_path=False):
    r"""
    Reports whether a uuid tree path is empty.

    :param uuidp:   a uuid or path
    :param root:    a root prefix directory (or None if there is no prefix)
    :param is_path: True if 'uuidp' should be interpreted as a path, otherwise
                    False.
    """

    if not is_path:
        dir_path = uuid_path(uuidp, root)
    else:
        dir_path = os.path.join(uuidp)

    files = os.path.join()


def gen_uuid():
    r"""
    This routine will generate a global uuid
    """

    unique_id = uuid.uuid4()
    return str(unique_id)


def parse_integer_list(str_list):
    r"""
    Parse a string in to list of integers. The input string can be one of the
    following forms:
        1) a comma separated list of integers, e.g. 1,2,3,4
        2) a start, end, step; e.g. 2:20:2
    If niether of these forms are matched, the None is returned.

    :param str_list: the string to be parsed in to a list of integers

    :return: the input comma separated list of inegers as an array of integers.
    """
    regex_range = re.compile(r'(\d+):(\d+):(\d+)')
    regex_comma_list = re.compile(r'(\d+)(,\s*\d+)*')

    match_range = regex_range.match(str_list)
    if match_range:
        start = int(match_range.group(1))
        stop = int(match_range.group(2))
        step = int(match_range.group(3))
        return list(range(start, stop, step))

    match_comma_list = regex_comma_list.match(str_list)
    if match_comma_list:
        return [int(si.strip()) for si in str_list.split(',')]

    return None


def model_sizes(root, material, geometry):
    r"""
    Retrieve the available sizes for a given material,geometry pair based on
    the legacy directory structure.

    :param root:     the legacy database directory tree root.
    :param material: the name of the material to import.
    :param geometry: the name of the geometry to import.

    :return: the available sizes for a given material/geometry pair (in the
             legacy data structure).
    """

    apg = os.path.join(root, material, geometry, 'lems')
    if not os.path.isdir(apg):
        raise IOError("The directory '{}' could not be found".format(apg))
    else:
        cnts = os.listdir(apg)
        szs = [int(d[:-2]) for d in cnts if os.path.isdir(os.path.join(apg,d))]

        return sorted(szs)


def model_temperatures(root, material, geometry, size):
    r"""
    Retrieve the available temperatures for a given material,geometry,size
    tuple based on the legacy directory structure.

    :param root:     the legacy database directory tree root.
    :param material: the name of the material to import.
    :param geometry: the name of the geometry to import.
    :param size:     the size (integer) of the model in nm.

    :return: the available temperatures for a given material/geometry/size (
             in the legacy data structure stored at `root`).
    """
    apt = os.path.join(root, material, geometry, 'lems', '{}nm'.format(size))
    if not os.path.isdir(apt):
        raise IOError("The directory '{}' could not be found".format(apt))
    else:
        cnts = os.listdir(apt)
        tps = [int(d[:-1]) for d in cnts if os.path.isdir(os.path.join(apt,d))]

        return sorted(tps)


def model_indices(root, material, geometry, size, temperature):
    r"""
    Retrieve the available indices for a given
    material,geometry,size,temperature tuple for the legacy directory
    structure, these indices correspond to tecplot solution files.

    :param root:        the legacy database directory tree root.
    :param material:    the name of the material to import.
    :param geometry:    the name of the geometry to import.
    :param size:        the size (integer) of the model in nm.
    :param temperature: the temperature (integer) of the model in C.

    :returns: the available set of idncides for a given
              material/geometry/size/temperature (in the legacy data structure)
    """
    regex_tecplot = re.compile(r'(\d+)nm_(\d+)C_mag_(\d+)_mult\.tec')
    apm = os.path.join(
        root,
        material,
        geometry,
        'lems',
        '{}nm'.format(size),
        '{}C'.format(temperature)
    )
    if not os.path.isdir(apm):
        raise IOError("The directory '{}' could not be found".format(apm))
    else:
        cnis = os.listdir(apm)
        tps = []
        for f in cnis:
            match_tecplot = regex_tecplot.match(f)
            if match_tecplot:
                tps.append(int(match_tecplot.group(3)))
        return sorted(tps)


def geometry_sizes(root, material, geometry):
    r"""
    Retrieve the available sizes for a given geometry for the legacy directory
    structure.

    :param root:     the legacy database directory tree root.
    :param material: the name of the material to import.
    :param geometry: the name of the geometry to import.

    :return: the sizes avaialable for a given geometry (stored in the legacy
             data structure at `root`).
    """
    regex_pat = re.compile(r'(\d+)\.pat')
    app = os.path.join(
        root,
        material,
        geometry,
        'meshes'
    )
    if not os.path.isdir(app):
        raise IOError("The directory '{}' could not be found".format(app))
    else:
        cnis = os.listdir(app)
        pps = []
        for f in cnis:
            match_pat = regex_pat.match(f)
            if match_pat:
                pps.append(int(match_pat.group(1)))
        return sorted(pps)


def convert_to_micron(size, unit, as_string=True, dp=3):
    r"""
    Converts the input size to microns, if str_version is set to true then a
    string version of the unit is produced.

    :param size:      the size (in floating point) to convert to micron
    :param unit:      the unit that `size` is given in
    :param as_string: whether to convert the size to a string (by default True)
    :param dp:        the number of decimal places to use for the output
                      string if `as_string` is set to true (by default 3)

    :returns: the input `size` float converted to micron.
    """
    if type(size) is str:
        fsize = float(size)
    else:
        fsize = size

    if unit == 'm':
        temp_size = 1E6*fsize
    elif unit == 'cm':
        temp_size = 1E4*fsize
    elif unit == 'mm':
        temp_size = 1E3*fsize
    elif unit == 'um':
        temp_size = size
    elif unit == 'nm':
        temp_size = 1E-3*fsize
    elif unit == 'fm':
        temp_size = 1E-6*fsize
    elif unit == 'am':
        temp_size = 1E-9*fsize
    else:
        raise ValueError("Unknown size unit")

    if as_string:
        frmt_str = '{{:20.{dp}f}}'.format(dp=dp)
        return (frmt_str.format(round(temp_size,dp))).strip()
    else:
        return round(temp_size, dp)


def convert_to_celsius(temperature, unit, as_string=True, dp=3):
    r"""
    Converts the input temperature to celsius

    :param temperature: the temperature (in floating point) to convert to micron
    :param unit:        the unit that `temperature` is given in
    :param as_string:   whether to convert the temperature to a string (by
                        default True)
    :param dp:          the number of decimal places to use for the output
                        string if `as_string` is set to true (by default 3)

    :returns: the input `temperature` float converted to micron.
    """
    if type(temperature) is str:
        ftemperature = float(temperature)
    else:
        ftemperature = temperature

    if unit == 'C':
        temp_temperature = ftemperature
    elif unit == 'K':
        temp_temperature = ftemperature - 273.15
    elif unit == 'F':
        temp_temperature = (ftemperature - 32.0)*(5.0/9.0)
    else:
        raise ValueError("Unknown temperature unit")

    if as_string:
        frmt_str = '{{:20.{dp}f}}'.format(dp=dp)
        return (frmt_str.format(round(temp_temperature,dp))).strip()
    else:
        return round(temp_temperature, dp)
