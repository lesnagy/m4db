import json
import datetime

from sqlalchemy.ext.declarative import DeclarativeMeta


def dump_material(obj):
    return dump_data(obj, {'anisotropy_form': dump_data})


def dump_materials(obj):
    materials = []
    for mma in obj:
        materials.append(
            dump_data(mma, {'material': dump_material})
        )
    return materials


def dump_geometry(obj):
    return dump_data(obj, {
        'size_unit': dump_data, 
        'size_convention': dump_data
    })


def dump_data(obj, recurse={}):
    if isinstance(obj.__class__, DeclarativeMeta):
        # an SQLAlchemy class
        fields = {}
        for field in [
                x for x in dir(obj)
                if not x.startswith('_') and x != 'metadata']:
            data = obj.__getattribute__(field)
            try:
                # this will fail on non-encodable values, like other classes
                json.dumps(data)
                fields[field] = data
            except TypeError:
                if field in recurse.keys():
                    fields[field] = recurse[field](data)
                else:
                    if isinstance(data, datetime.datetime):
                        fields[field] = data.strftime("%m/%d/%Y, %H:%M:%S")
                    else:
                        fields[field] = None

        # a json-encodable dict
        return fields

    return None


class DefaultAlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        data = dump_data(obj)

        if data is None:
            return json.JSONEncoder.default(self, obj)
        else:
            return data


class DefaultModelEncoder(json.JSONEncoder):

    def default(self, obj):
        data = dump_data(obj,
            {
                'geometry': dump_geometry,
                'running_status': dump_data,
                'start_magnetization': dump_data,
                'materials': dump_materials
            }
        )

        if data is None:
            return json.JSONEncoder.default(self, obj)
        else:
            return data


class DefaultNebEncoder(json.JSONEncoder):

    def default(self, obj):
        data = dump_data(obj,
            {
                'start_model': dump_data,
                'end_model': dump_data
            }
        )

        if data is None:
            return json.JSONEncoder.default(self, obj)
        else:
            return data


class DefaultGeometryEncoder(json.JSONEncoder):

    def default(self, obj):
        data = dump_data(obj,
            {
                'size_convention': dump_data,
                'size_unit': dump_data
            }
        )

        if data is None:
            return json.JSONEncoder.default(self, obj)
        else:
            return data