r"""
This module deals with retrieving data from m4db remotely.
"""

from m4db.rest.client.model import get_model_zip
from m4db.rest.client.model import get_model_metadata_by_unique_id
from m4db.global_config import get_global_variables

from m4db.util import uuid_path
from m4db.util import clear_directory

from zipfile import ZipFile

from datetime import datetime

import os


def get_local_model_dir():
    gcfg = get_global_variables()

    local_model_dir = os.path.join(
        gcfg.database.file_root,
        'models'
    )

    # Make the directory if it does not exist
    if not os.path.isdir(local_model_dir):
        os.makedirs(local_model_dir, exist_ok=True)

    return local_model_dir


def cache_model_zip(unique_id):
    gcfg = get_global_variables()

    local_model_dir = os.path.join(
        get_local_model_dir(),
        uuid_path(unique_id)
    )
    if not os.path.isdir(local_model_dir):
        os.makedirs(local_model_dir)

    zip_file = os.path.join(
        local_model_dir,
        "{}.zip".format(unique_id)
    )
    if os.path.isfile(zip_file):
        # The zip file exists, so check the creation date to see if we need to get a more recent version
        metadata = get_model_metadata_by_unique_id(unique_id)
        server_created_datetime = datetime.strptime(metadata['last_modified'], gcfg.date_time_format)
        local_created_datetime = datetime.fromtimestamp(os.path.getmtime(zip_file))

        if local_created_datetime < server_created_datetime:
            # If the local file's creation time is before the one on the server
            clear_directory(local_model_dir)
            get_model_zip(unique_id, local_model_dir)
            zf = ZipFile(zip_file, 'r')
            zf.extractall(path=local_model_dir)
    else:
        # The zip file does not exist, so retrieve it
        get_model_zip(unique_id, local_model_dir)
        zf = ZipFile(zip_file, 'r')
        zf.extractall(path=local_model_dir)

    return local_model_dir

