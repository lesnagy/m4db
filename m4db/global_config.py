import configparser
import logging
import os
import sys


class DatabaseConfiguration:
    r"""
    Configuration information for database connections.
    """
    def __init__(self):
        self.url_template = None
        self.user = None
        self.host = None
        self.db = None
        self.db_uuid = None
        self.file_root = None
        self.work_root = None

    @staticmethod
    def new(config_file):
        r"""
        Set up data from the input config file
        :param config_file: configuration file name
        :return: None
        """
        config = configparser.RawConfigParser()
        config.read(config_file)
        db_config = DatabaseConfiguration()
        if "database" in config:
            if "url-template" in config["database"]:
                db_config.url_template = config["database"]["url-template"]
            else:
                raise ValueError("Could not find key 'url-template' in section 'database'")
            if "user" in config["database"]:
                db_config.user = config["database"]["user"]
            else:
                raise ValueError("Could not find key 'user' in section 'database'")
            if "host" in config["database"]:
                db_config.host = config["database"]["host"]
            else:
                raise ValueError("Could not find key 'host' in section 'database'")
            if "db" in config["database"]:
                db_config.db = config["database"]["db"]
            else:
                raise ValueError("Could not find key 'db' in section 'database'")
            if "db-uuid" in config["database"]:
                db_config.db_uuid = config["database"]["db-uuid"]
            else:
                raise ValueError("Could not find key 'db-uuid' in section 'database'")
            if "file-root" in config["database"]:
                db_config.file_root = config["database"]["file-root"]
            else:
                raise ValueError("Could not find key 'file-root' in section 'database'")
            if "work-root" in config["database"]:
                db_config.work_root = config["database"]["work-root"]
            else:
                raise ValueError("Could not find key 'work-root' in section 'database'")
        else:
            raise ValueError("Could not find section 'database'")
            sys.exit(1)
        return db_config


class ExternalConfiguration:
    r"""
    Configuration information for external programs
    """
    def __init__(self):
        self.mm_solver = None

    @staticmethod
    def new(config_file):
        r"""
        Set up data from the input config file
        :param config_file: configuration file name
        :return: None
        """
        config = configparser.RawConfigParser()
        config.read(config_file)
        ext_config = ExternalConfiguration()
        if "external" in config:
            if "mm-solver" in config["external"]:
                ext_config.mm_solver = config["external"]["mm-solver"]
            else:
                raise ValueError("Could not find key 'mm-solver' in section 'external'")
        else:
            raise ValueError("Could not find section 'external'")
        return ext_config


class WebServiceConfiguration:
    r"""
    Configuration information for web services.
    """
    def __init__(self):
        self.url = None
        self.port = None
        self.cert = None
        self.retries = None
        self.backoff = None
        self.forcelist = None

    @staticmethod
    def new(config_file):
        r"""
        Set up data from the input config file
        :param config_file: configuration file name
        :return: None
        """
        config = configparser.RawConfigParser()
        config.read(config_file)
        wsrv_config = WebServiceConfiguration()
        if "web-service" in config:
            if "url" in config["web-service"]:
                wsrv_config.url = config["web-service"]["url"]
            else:
                raise ValueError("Could not find key 'url' in section 'web-service'")
            if "port" in config["web-service"]:
                wsrv_config.port = config["web-service"]["port"]
            else:
                raise ValueError("Could not find key 'port' in section 'web-service'")
            if "cert" in config["web-service"]:
                wsrv_config.cert = config["web-service"]["cert"]
            else:
                raise ValueError("Could not find key 'cert' in section 'web-service'")
            if "retries" in config["web-service"]:
                wsrv_config.retries = config["web-service"]["retries"]
            else:
                raise ValueError("Could not find key 'retries' in section 'web-service'")
            if "backoff" in config["web-service"]:
                wsrv_config.backoff = config["web-service"]["backoff"]
            else:
                raise ValueError("Could not find key 'backoff' in section 'web-service'")
            if "forcelist" in config["web-service"]:
                wsrv_config.forcelist = config["web-service"]["forcelist"]
            else:
                raise ValueError("Could not find key 'forcelist' in section 'web-service'")
        else:
            raise ValueError("Could not find section 'web-service'")
        return wsrv_config


class Configuration:
    r"""
    Global configuration object.
    """
    def __init__(self):
        self.m4db_root = os.getenv('M4DB_ROOT')
        self.m4db_version = os.getenv('M4DB_VERSION')
        if self.m4db_root is None or self.m4db_version is None:
            raise ValueError("Environment variables M4DB_ROOT, M4DB_VERSION need to be defined")
        self.config_file = os.path.join(self.m4db_root, "config", self.m4db_version)
        if not os.path.isfile(self.config_file):
            raise ValueError("Could not find configuration file: '{}'".format(self.config_file))
        self.home = os.path.join(self.m4db_root, "application", self.m4db_version)
        
        # Standard file names
        self.model_script_merrill = 'model_script.merrill'
        self.energy_script_merrill = 'energy_script.merrill'
        self.neb_script_merrill = 'neb_script.merrill'
        self.magnetization_dat = 'magnetization.dat'
        self.magnetization_tec = 'magnetization.tec'
        self.neb_tec = 'neb.tec'
        self.magnetization_mult_tec = 'magnetization_mult.tec'
        self.magnetization_plus_tec = 'magnetization_plus.tec'
        self.magnetization_log = 'magnetization.log'
        self.magnetization = 'magnetization'
        self.model_stderr_txt = 'model_stderr.txt'
        self.model_stdout_txt = 'model_stdout.txt'
        self.neb_stderr_txt = 'neb_stderr.txt'
        self.neb_stdout_txt = 'neb_stdout.txt'
        self.energy_stderr_txt = 'energy_stderr.txt'
        self.energy_stdout_txt = 'energy_stdout.txt'
        self.geometry_cubit = 'geometry.cubit'
        self.geometry_pat = 'geometry.pat'
        self.geometry_e = 'geometry.e'
        self.geometry_stdout = 'geometry.stdout'
        self.slurm_sh = 'slurm.sh'
        self.geometry_dir_name = 'geometry'
        self.model_dir_name = 'model'
        self.neb_dir_name = 'neb'
        self.hysteresis_dir_name = 'hysteresis'
        self.forc_dir_name = 'forc'
        self.cron_dir_name = 'cron'
        self.dot_m4db = ".m4db"
        self.base_data = 'base_data'
        self.templates = 'templates'
        self.report = 'report'
        self.templates_report_dir = os.path.join(
            self.m4db_root,
            "application",
            self.m4db_version,
            "base_data",
            "templates",
            "report"
        )
        self.model_summary_txt_jinja2 = 'model_summary_txt.jinja2'
        self.model_summary_stdout_jinja2 = 'model_summary_stdout.jinja2'
        self.geometry_summary_stdout_jinja2 = 'geometry_summary_stdout.jinja2'
        self.neb_summary_stdout_jinja2 = 'neb_summary_stdout.jinja2'
        self.models_mgst_quants_stdout_jinja2 = 'models_mgst_quants_stdout.jinja2'
        self.merrill_energies_by_material_temperature_jinja2 = 'merrill_energies_by_material_temperature.jinja2'
        self.merrill_energies_jinja2 = 'merrill_energies.jinja2'
        self.merrill_model_jinja2 = 'merrill_model.jinja2'
        self.merrill_neb_child_path_jinja2 = 'merrill_neb_child_path.jinja2'
        self.merrill_neb_root_path_jinja2 = 'merrill_neb_root_path.jinja2'
        self.slurm_model_jinja2 = 'slurm_model.jinja2'
        self.slurm_neb_jinja2 = 'slurm_neb.jinja2'
        self.model_summary_txt = 'model_summary.txt'
        self.model_summary_json = 'model_summary.json'

        # Additional information
        self.lst_size_units = ['m', 'cm', 'mm', 'um', 'nm', 'pm', 'fm', 'am']

        # Some useful regular expressions
        self.rstr_hex = r'[0-9A-Fa-f]'
        self.rstr_float = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
        self.rstr_uuid = \
            '{hx:}{{8}}-{hx:}{{4}}-{hx:}{{4}}-{hx:}{{4}}-{hx:}{{12}}'.format(
                hx=self.rstr_hex
            )

        # Some useful data formats
        self.date_time_format = "%m/%d/%Y, %H:%M:%S"

        # Database configuration info
        self.database = DatabaseConfiguration.new(self.config_file)
        self.geometry_dir = os.path.join(self.database.file_root, self.geometry_dir_name)
        self.model_dir = os.path.join(self.database.file_root, self.model_dir_name)
        self.neb_dir = os.path.join(self.database.file_root, self.neb_dir_name)
        self.hysteresis_dir = os.path.join(self.database.file_root, self.hysteresis_dir_name)
        self.forc_dir = os.path.join(self.database.file_root, self.forc_dir_name)
        self.cron_dir = os.path.join(self.database.file_root, self.cron_dir_name)

        # External program configuration info
        self.external = ExternalConfiguration.new(self.config_file)

        # Web service configuration info
        self.web_service = WebServiceConfiguration.new(self.config_file)

        # Logging
        self.logger = self.setup_logger()

    def setup_logger(self):
        r"""
        Create a logger object based on information the configuration file
        :return: a logger object
        """
        config = configparser.RawConfigParser()
        config.read(self.config_file)
        if "logging" in config:
            if "level" in config["logging"]:
                log_level = getattr(logging, config["logging"]["level"].upper())
            else:
                raise ValueError("Could not find key 'level' in section 'logging'")
            if "destination" in config["logging"]:
                log_destination = config["logging"]["destination"]
            else:
                raise ValueError("Could not find key 'destination' in section 'logging'")
            if "format" in config["logging"]:
                log_format = config["logging"]["format"]
            else:
                raise ValueError("Could not find key 'format' in section 'logging'")
        else:
            raise ValueError("Could not find section 'logging'")
        logger = logging.getLogger('m4db')
        logger.setLevel(log_level)
        if log_destination != 'stdout':
            log_handler = logging.FileHandler(log_destination)
        else:
            log_handler = logging.StreamHandler()
        log_formatter = logging.Formatter(log_format.replace('<newline>', '\n'))  # hack: allows new lines in log output
        log_handler.setLevel(log_level)
        log_handler.setFormatter(log_formatter)
        logger.addHandler(log_handler)
        return logger

    def display_environment(self):
        r"""
        Displays all the environment variables stored by this object.
        :return: None
        """
        print("TODO: Implementation required")


def get_global_variables():
    if get_global_variables.gcfg is None:
        get_global_variables.gcfg = Configuration()
    return get_global_variables.gcfg


get_global_variables.gcfg = None
