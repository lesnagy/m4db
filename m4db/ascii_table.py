from decimal import Decimal


class AsciiTable:
    r"""
    Class to draw ascii tables.
    """
    def __init__(self, *cols):
        self._nrows = 0
        self._ncols = len(cols)
        self._headings = [''] * self._ncols
        self._data = []
        self._barBelow = []
        self._barAbove = []
        self._maxColWidths = [0] * self._ncols

        self._defaultBlank = 'n/a'
        self._defaultFloatFormat = '{:020.15f}'
        self._defaultIntFormat = '{:d}'

        self._joint = '+'
        self._bar = '|'
        self._horiz = '-'

        self._nohead = False

        self.set_headings(*cols)

    def __str__(self):
        # A table frame.
        lst_frame_bar = []
        lst_row_separator = []
        lst_data_row = []
        for i in range(len(self._maxColWidths)):
            w = self._maxColWidths[i]
            lst_frame_bar.append(self._horiz * (w + 2))
            lst_row_separator.append(self._horiz * (w + 2))
            lst_data_row.append(' {{{}:<{}}} '.format(i, w))
        frame_bar = self._joint + self._joint.join(lst_frame_bar) + self._joint
        data_row = self._bar + self._bar.join(lst_data_row) + self._bar

        # Create string version of the table.
        str_table = ''
        str_table += frame_bar + '\n'
        if not self._nohead:
            str_table += data_row.format(*self._headings) + '\n'
            str_table += frame_bar + '\n'

        for i in range(len(self._data)):
            item = self._data[i]
            if self._barAbove[i]:
                str_table += frame_bar + '\n'
            str_table += data_row.format(*item) + '\n'
            if self._barBelow[i]:
                str_table += frame_bar + '\n'
        str_table += frame_bar + '\n'

        return str_table

    def nrows(self):
        return self._nrows

    def ncols(self):
        return self._ncols

    def add_row(self, *args, **kwargs):

        # Parse whether we want bars above/below our row.
        bar_below = False
        bar_above = False

        for key, value in kwargs.items():
            if key == 'bar_below':
                bar_below = value
            if key == 'bar_above':
                bar_above = value

        new_row = []

        # Check that the new row has correct number of elements, truncate/fill with defaults if necessary
        if len(args) != self.ncols():
            # If the user gives more arguments than columns then truncate.
            if len(args) > self.ncols():
                for i in range(self.ncols()):
                    new_row.append(args[i])
            # If the user gives less arguments than columns then fill.
            elif len(args) < self.ncols():
                for i in range(self.ncols()):
                    if i < len(args):
                        new_row.append(args[i])
                    else:
                        new_row.append(self._defaultBlank)
        elif len(args) == self.ncols():
            # If the user gives the same data as no. of colums, copy over.
            for v in args:
                new_row.append(v)

        # Check that the new row is all string data, if not do the correct conversion
        for i in range(len(new_row)):
            element = new_row[i]
            if type(element) is not str:
                if type(element) is float:
                    new_row[i] = self._defaultFloatFormat.format(element)
                elif type(element) is int:
                    new_row[i] = self._defaultIntFormat.format(element)
                elif type(element) is Decimal:
                    new_row[i] = str(element)
                elif type(element) is bool:
                    new_row[i] = 'true' if element else 'false'
                elif element is None:
                    new_row[i] = ''
                else:
                    raise AsciiTableNonStringException(
                        'Type of element {0} should be {1}, but was {2}'.format(
                            i, 'str', type(element)))

        # Add row
        self._data.append(new_row)
        self._barBelow.append(bar_below)
        self._barAbove.append(bar_above)

        # Increment number of rows
        self._nrows += 1
        # Update column widths if necessary
        self.update_max_column_widths()

    def set_headings(self, *args):
        # Check that the new headings has correct number of elements
        if len(args) != self.ncols():
            raise AsciiTableRowLengthMismatchException(
                'Expected `args` length {0}, but length was {1}'.format(
                    self.ncols(), len(args)))
        # Check that the new heading is all string data
        for i in range(len(args)):
            element = args[i]
            if type(element) is not str:
                raise AsciiTableNonStringException(
                    'Type of element {0} in `args` should be {1}, but was {2}'.format(
                        i, 'str', type(element)))
        # Update headings
        self._headings = args
        # Update column widths if necessary
        self.update_max_column_widths()

    def update_max_column_widths(self):
        # Reset to zero.
        self._maxColWidths = [0] * self._ncols

        # First use headings for widths.
        for i in range(self.ncols()):
            heading = self._headings[i]
            if self._maxColWidths[i] < len(heading):
                self._maxColWidths[i] = len(heading)

        # Check through each data item.
        for j in range(self.ncols()):
            for i in range(self.nrows()):
                data = self._data[i][j]
                if self._maxColWidths[j] < len(data):
                    self._maxColWidths[j] = len(data)


class AsciiTableRowLengthMismatchException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AsciiTableNonStringException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


if __name__ == '__main__':
    # Create a table with four columns
    tbl1 = AsciiTable(4)

    # Give the table headings
    tbl1.set_headings('one', 'two', 'three', 'four')

    # Add two rows of data (they should all be strings)
    tbl1.add_row('mumra', 'the', 'ever', 'living')
    tbl1.add_row(
        '{0:20.15f}'.format(19.1919191),
        '{0:20.15f}'.format(20.11339),
        '{0:10.5f}'.format(12.11939399),
        '{0:20.15f}'.format(.9399)
    )
    tbl1.add_row(19.1919191, 20.11339, 12.11939399, 0.9399)
    tbl1.add_row(12993.12342, 10, 'hello', 'world')
    tbl1.add_row('one', 'two', 'three', barAbove=True, barBelow=True)
    tbl1.add_row(129.142, 10, 20, 30)
    tbl1.add_row(129.142, 10, 20, 30, 40, 50, 60)

    print(tbl1)
