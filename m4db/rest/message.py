r"""
This module defines a set of simple POD objects that may be used to communicate
back and forth with the http service.
"""

import inspect
import json

ERROR = 'ERROR'
SUCCESS = 'SUCCESS'


def as_string(pod):
    r"""
    Produce a string version of a pod.
    """
    attributes = []
    for attribute in [a for a in dir(pod)
                      if a is not 'None' and
                         not a.startswith('__') and
                         not inspect.ismethod(getattr(pod, a))
                      ]:
        attributes.append(attribute)

    max_attr_len = 0
    for attribute in attributes:
        if len(attribute) > max_attr_len:
            max_attr_len = len(attribute)

    format_str = '{{:{}s}} : {{}}'.format(max_attr_len)
    str_pod = '\n'.join(
        [format_str.format(attr, getattr(pod, attr)) for attr in attributes]
    )

    return str_pod


def as_dict(pod):
    r"""
    Convert the input pod object to a python dictionary.

    :param pod: the pod object that is to be converted to python dictionary

    :return: a dictionary version of the input pod, where dictionary keys
             correspond to pod attributes.
    """

    dict_data = {}
    for attribute in [a for a in dir(pod)
                      if a is not 'None' and
                         not a.startswith('__') and
                         not inspect.ismethod(getattr(pod, a))
                      ]:
        dict_data[attribute] = getattr(pod, attribute)

    return dict_data


def as_json(pod):
    r"""
    Convert the input pod object to a JSON string.

    :param pod: the pod object that is to be converted to JSON string

    :return: a JSON stringversion of the input pod, where dictionary keys
             correspond to pod attributes.
    """

    dict_data = pod.as_dict()

    return json.dumps(dict_data)


def check_dict(pod, dict_data):
    r"""
    Check that the POD object `pod` and python dictionary `dict_data` are
    valid, i.e. that the variable attributes of `pod` appear in `dict_data`
    and vice versa.
    """
    # Check that the attributes of the pod appear in the dictionary
    attributes = []
    for attribute in [a for a in dir(pod)
                      if a is not 'None' and
                         not a.startswith('__') and
                         not inspect.ismethod(getattr(pod, a))
                      ]:
        attributes.append(attribute)
        if attribute not in dict_data.keys():
            raise ValueError(
                "Expected attribute '{}' in dictionary".format(attribute)
            )

    # Check that the keys of the dictionary have corresponding attributes
    for key in dict_data.keys():
        if key not in attributes:
            raise ValueError(
                "Expected key '{}' in POD".format(key)
            )


def from_dict(pod, dict_data):
    r"""
    Read python dictionary in to a pod object.

    :param pod:       a reference to the pod object that `pdict` will be saved to
    :param dict_data: the python dictionary to read in to `pod`
    """
    # Check that the pod and dict are 'compatible'
    pod.check_dict(dict_data)

    # Read over the data
    for key, value in dict_data.items():
        setattr(pod, key, value)


def from_json(pod, json_string):
    r"""
    Read JSON in to a pod object.

    :param pod:         a reference to the pod object that `json_data` will be saved to
    :param json_string: the JSON to read in to `pod`
    """
    dict_data = json.loads(json_string)
    pod.from_dict(dict_data)


def create_response_pod(name, attributes):
    r"""
    Create a response POD class. HTTP services will return these types of
    object as a response to a request.

    :param name:       the name of the new response POD class.
    :param attributes: the attributes of the new class

    :return: a new class with a set of attributes that simplify communication
             with the http service.
    """

    # Copy over attributes
    new_attributes = {}
    for key, value in attributes.items():
        new_attributes[key] = value

    # Every response message must always have these attributes set
    new_attributes['status'] = None

    ###########################################################################
    # Methods                                                                 #
    ###########################################################################

    # Add a method to check that a dicionary is valid with the pod
    new_attributes['check_dict'] = check_dict

    # Add a method to produce a python dictionary version of the pod
    new_attributes['as_dict'] = as_dict

    # Add a method to produce JSON version of the pod
    new_attributes['as_json'] = as_json

    # Add a method to read a dict in to the pod
    new_attributes['from_dict'] = from_dict

    # Add a method to read a JSON in to the pod
    new_attributes['from_json'] = from_json

    # Add a method to produce a string version of the object
    new_attributes['__str__'] = as_string

    # Return the type
    return type(name, (), new_attributes)


def create_request_pod(name, attributes):
    r"""
    Create a request POD class. HTTP services will accept JSON versions of
    objects of these types which they will use to construct queries/perform
    updates etc.

    :param name:       the name of the new request POD class.
    :param attributes: the attributes of the new class.

    :return: a new class with the given set of attributes to simplify
             communication with the http service.
    """

    # Copy over attributes
    new_attributes = {}
    for key, value in attributes.items():
        new_attributes[key] = value

    ###########################################################################
    # Methods                                                                 #
    ###########################################################################

    # Add a method to check that a dicionary is valid with the pod
    new_attributes['check_dict'] = check_dict

    # Add a method to produce a python dictionary version of the pod
    new_attributes['as_dict'] = as_dict

    # Add a method to produce JSON version of the pod
    new_attributes['as_json'] = as_json

    # Add a method to read a dict in to the pod
    new_attributes['from_dict'] = from_dict

    # Add a method to read a JSON in to the pod
    new_attributes['from_json'] = from_json

    # Add a method to produce a string version of the object
    new_attributes['__str__'] = as_string
    return type(name, (), new_attributes)


# POD objects used to send data back and forth between http service

###############################################################################
# Response POD messages, these messages are recieved *FROM* the http service  #
###############################################################################

ResponseMsgBasic = create_response_pod('ResponseMsgBasic', {'message': ''})
r"""
A basic response that will simpy report success/or failure and give a message.
"""

###############################################################################
# Request POD messages, these messages are sent *TO* the http service         #
###############################################################################
RequestMsgCluster = create_request_pod('RequestMsgCluster', {
    'geometry': '',
    'size': 0.0,
    'size_unit': 'um',
    'temperature': 0.0,
    'temperature_unit': 'C',
    'running_status': '',
    'user_name': '',
    'project_name': '',
    'algo_name': ''
})
r'''
Parameters sent to the
    ``/cluster`` service retrieve cluster information
'''

RequestMsgModelQuants = create_request_pod('RequestMsgModelQuants', {
    'unique_id': '',
    'mx_tot': 0.0,
    'my_tot': 0.0,
    'mz_tot': 0.0,
    'vx_tot': 0.0,
    'vy_tot': 0.0,
    'vz_tot': 0.0,
    'h_tot': 0.0,
    'adm_tot': 0.0,
    'e_typical': 0.0,
    'e_anis': 0.0,
    'e_ext': 0.0,
    'e_demag': 0.0,
    'e_exch1': 0.0,
    'e_exch2': 0.0,
    'e_exch3': 0.0,
    'e_exch4': 0.0,
    'e_tot': 0.0
})
r"""
Parameters to send to the
    ``/model/update-model-quants`` service that updates the quantities (quants)
                                   associated with a :class:`~m4db.orm.Model`
                                   object.
"""


ModelMetadata = create_request_pod('ModelMetadata', {
    'unique_id': '',
    'created': '',
    'last_modified': '',
    'db_user': '',
    'project_name': '',
    'generated_by': ''
})
r"""
A set of data containing some useful metadata about a model.
"""


RequestMsgSetRunningStatus = create_request_pod('RequestMsgSetRunningStatus', {
    'unique_id': '',
    'new_status': ''
})
r'''
Parameters to send to the
    ``/model/set-running-status`` service that simply changes the running status
                                  of and existing model without regard for
                                  what the old status was.
'''
