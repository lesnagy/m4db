r"""
This is a client side module that makes REST-ish requests to m4db's http
http service. The functions in ths module focus around querying and
updating micromagnetic model user data.
"""

import os
import json

# Suppress warnings about insecure requests
import urllib3

import m4db.http_utils

from m4db.global_config import get_global_variables

from m4db.rest.message import RequestMsgSetRunningStatus

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

########################################################################
# These functions only interact with http services that query          #
########################################################################

def get_all_users():
    r"""
    This gets a list of all user identities on the database
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/allusers'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)





def set_new_user(
        user_name, first_name, surname, email,
       initials=None, telephone=None ):
    r"""
    Sets the identity of a new user .
    :param user_name:
    :param first_name:
    :param surname:
    :param email:
    :param initials:
    :param telephone:

    :returns: a python dictionary containing new user information.
    """
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()
    new_user = {"user_name":user_name, "first_name":first_name, "surname":surname,
                "email":email}
    if initials is not None:
        new_user["initials"]=initials

    if telephone is not None:
        new_user["telephone"]=telephone

    response = session.post(
        'https://{url:}:{port:}/newuser'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=json.dumps(new_user),
        verify=False
    )
    response.raise_for_status()

if __name__ == "__main__":
    allusers=get_all_users()
    print(allusers)
    set_new_user( "jennny10", "jenny", "harris", "J@m.com", telephone="0783874645")
