r"""
This is a client side module that makes REST requests to m4db's http
http service. The functions in ths module focus around querying and
updating micromagnetic NEB data.
"""
import json
import requests

import m4db.http_utils
from m4db.global_config import get_global_variables
from m4db.rest.message import RequestMsgSetRunningStatus

# Suppress warnings about insecure requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

########################################################################
# These functions interact with http services that don't make CHANGE   #
########################################################################
def get_neb_by_unique_id(unique_id):
    r"""
    Retrieves the information associated with a neb by talking to the
    http service.
    :param unique_id: the unique id of a neb (which is always of the form
                ``12345678-1234-5678-1234-567812345678``)
    :returns: a python dictionary containing neb information
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/neb/{unique_id}'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port,
            unique_id=unique_id
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)


########################################################################
# These functions interact with http services that cause CHANGE        #
########################################################################
def get_path_root_by_start_end_unique_ids(unique_id_one, unique_id_two):
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()

    response = session.get(
        "https://{url:}:{port:}/neb/get-path-root-by-start-end-unique-ids/{unique_id_one:}/{unique_id_two:}".format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port,
            unique_id_one=unique_id_one,
            unique_id_two=unique_id_two
        ),
        verify=False
    )
    if response.status_code == 404:
        return None
    else:
        response.raise_for_status()
        return json.loads(response.text)


def add_not_run(
        spring_constant, curvature_weight, no_of_points, start_unique_id, end_unique_id,
        parent_unique_id, calculation_type, user_name, project_name):
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    new_neb_msg = {
        'spring_constant': spring_constant,
        'curvature_weight': curvature_weight,
        'no_of_points': no_of_points,
        'start_unique_id': start_unique_id,
        'end_unique_id': end_unique_id,
        'parent_unique_id': parent_unique_id,
        'calculation_type': calculation_type,
        'user_name': user_name,
        'project_name': project_name
    }
    response = session.post(
        'https://{url:}:{port:}/neb/add-not-run'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=json.dumps(new_neb_msg),
        verify=False
    )
    response.raise_for_status()
    return json.loads(response.text)


def set_running_status(uid, new_status):
    r"""
    Sets the running status of a NEB.

    :param uid:        the unique id of a NEB (which is always of the form
                       ``12345678-1234-5678-1234-567812345678``)
    :param new_status: the name of the new status to which the NEB should
                       be set (a complete set of running statuses may be
                       retrieved with
                       :func:`~neb.rest.client.running_status.get_all()`

    :returns: a python dictionary containing running status information.
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    request_msg = RequestMsgSetRunningStatus()
    request_msg.unique_id = uid
    request_msg.new_status = new_status
    response = session.post(
        'https://{url:}:{port:}/neb/set-running-status'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=request_msg.as_json(),
        verify=False
    )
    response.raise_for_status()
