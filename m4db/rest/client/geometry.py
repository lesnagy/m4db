r'''
This is a client side module that makes REST requests to m4db's http
http service. The functions in ths module focus around querying and and
updating micromagnetic geometry data.
'''

import json

# Suppress warnings about insecure requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import m4db.http_utils

from m4db.global_config import get_global_variables


########################################################################
# These functions interact with http services that don't make CHANGE   #
########################################################################
def get_geometry_by_unique_id(unique_id):
    r"""
    Retrieves the information associated with a neb by talking to the
    http service.
    :param unique_id: the unique id of a neb (which is always of the form
                ``12345678-1234-5678-1234-567812345678``)
    :returns: a python dictionary containing neb information
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/geometry/{unique_id}'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port,
            unique_id=unique_id
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)

def get_geometries():
    r'''
    Talk to the http service and retrieve a list of geometries.
    '''
    
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()

    endpoint = 'https://{url:}:{port:}/geometry/all'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)
