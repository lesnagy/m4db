"""
This is a client side module that makes REST-ish requests to m4db's http
http service. The functions in ths module focus around querying and
updating micromagnetic model software versions.
"""

import os
import json

# Suppress warnings about insecure requests
import urllib3

import m4db.http_utils

from m4db.global_config import get_global_variables

from m4db.rest.message import RequestMsgSetRunningStatus

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

########################################################################
# These functions only interact with http services that query          #
########################################################################

def get_all_softwares():
    r"""
    This gets a list of all user identities on the database
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/allsoftwares'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)


def set_new_software(
        name, version,
        description=None, url=None, citation=None):
    r"""
    Sets the identity of a new user .
    :param name             : name of the software package
    :param version          : the version of the software package
    :param description      : a description of the software package
    :param url              : a URL of the software package
    :param citation               : a citation for the software package
    :returns: a python dictionary containing new user information.
    """
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()
    new_software = {"name":name, "version":version}
    if description is not None:
        new_software["description"]=description

    if url is not None:
        new_software["url"]=url

    if citation is not None:
        new_software["citation"]=citation

    response = session.post(
        'https://{url:}:{port:}/newsoftware'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=json.dumps(new_software),
        verify=False
    )
    response.raise_for_status()

if __name__ == "__main__":
    allsoftwares=get_all_softwares()
    print(allsoftwares)
    set_new_software( "IGOR", "1.3", citation="J. Silly Names.")


