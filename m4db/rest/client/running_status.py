import json


# Suppress warnings about insecure requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import m4db.http_utils

from m4db.global_config import get_global_variables


def get_all():
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()

    response = session.get(
        'https://{url:}:{port:}/running-status/all'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        verify=False
    )

    response.raise_for_status()

    return json.loads(response.text)


