r"""
This is a client side module that makes REST-ish requests to m4db's http
http service. The functions in ths module focus around querying and
updating micromagnetic model data.
"""

import os
import json

# Suppress warnings about insecure requests
import urllib3

import m4db.http_utils

from m4db.global_config import get_global_variables

from m4db.rest.message import RequestMsgSetRunningStatus

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

########################################################################
# These functions only interact with http services that query          #
########################################################################


def get_model_by_unique_id(unique_id):
    r"""
    Retrieves the information associated with a model by talking to the
    http service.
    :param unique_id: the unique id of a model (which is always of the form
                ``12345678-1234-5678-1234-567812345678``)
    :returns: a python dictionary containing model information
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/model/{unique_id}'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port,
            unique_id=unique_id
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)


def get_model_metadata_by_unique_id(unique_id):
    r"""
    Retrieves model metadata by talking to the http service.
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()

    endpoint = 'https://{url:}:{port:}/model-metadata/{unique_id:}'.format(
        url=gcfg.web_service.url,
        port=gcfg.web_service.port,
        unique_id=unique_id
    )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)
    

def get_model_zip(unique_id, directory=None):
    r"""
    Retrieves lots of information about the model (its structure
    and other goodies) as a zipped archive.
    :param unique_id: the unique id of a model, this function will proceed to write the output zip file to the
                      user's current directory.
    :param directory: the directory in to which the resulting zip is to be place.
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    
    endpoint = 'https://{url:}:{port:}/model-zip/{unique_id:}.zip'.format(
        url=gcfg.web_service.url,
        port=gcfg.web_service.port,
        unique_id=unique_id
    )
    
    response = session.get(endpoint, stream=True, verify=False)
    response.raise_for_status()

    if directory is None:
        local_filename = '{unique_id:}.zip'.format(unique_id=unique_id)
    else:
        local_filename = os.path.join(
            directory,
            '{unique_id:}.zip'.format(unique_id=unique_id)
        )
    
    with open(local_filename, 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            f.write(chunk)


def get_models_by_material_geometry_size_temperature(
        run_stat, user_name, proj_name,
        material, geometry,
        size, size_unit, size_conv,
        temp, temp_unit):
    r"""
    Retrieve a collection of model data using the input parameters.
    :param run_stat: the m4db running status of the model 'e.g. finished'
    :param user_name: the m4db user name that the models should belong to
    :param proj_name: the m4db project that the models should belong to
    :param material: the material name that the models should belong to
    :param geometry: the geometry name that the models should belong to
    :param size: the geometry size
    :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
    :param size_conv: the size convention ('esvd' or 'ecvl')
    :param temp: the temperature
    :param temp_unit: the temperature unit
    :return: a python dictionary containing model data
    """
    
    gcfg = get_global_variables()
    
    session = m4db.http_utils.get_retry_session()
    
    endpoint = 'https://{url:}:{port:}/models/{run_stat:}/{user_name:}/{proj_name:}/{material:}/{geometry:}/{size:}/{size_unit:}/{size_conv:}/{temp:}/{temp_unit:}'.format(
        url=gcfg.web_service.url,
        port=gcfg.web_service.port,
        run_stat=run_stat,
        user_name=user_name,
        proj_name=proj_name,
        material=material,
        geometry=geometry,
        size=size,
        size_unit=size_unit,
        size_conv=size_conv,
        temp=temp,
        temp_unit=temp_unit
    )
    
    response = session.get(endpoint, verify=False)
    response.raise_for_status()
    
    return json.loads(response.text)


def get_model_quants_by_material_geometry_size_temperature(
        run_stat, user_name, proj_name,
        material, geometry,
        size, size_unit, size_conv,
        temp, temp_unit):
    r"""
    Retrieve model quantities using input parameters.
    :param run_stat:  the m4db running status of the model 'e.g. finished'
    :param user_name: the m4db user name that the models should belong to
    :param proj_name: the m4db project that the models should belong to
    :param material:  the material name that the models should belong to
    :param geometry:  the geometry name that the models should belong to
    :param size:      the geometry size
    :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
    :param size_conv: the size convention ('esvd' or 'ecvl')
    :param temp:      the temperature
    :param temp_unit: the temperature unit
    :return: a python dictionary containing model quantities data
    """
    
    gcfg = get_global_variables()
    
    session = m4db.http_utils.get_retry_session()
    
    endpoint = 'https://{url:}:{port:}/model-quants/{run_stat:}/{user_name:}/{proj_name:}/{material:}/{geometry:}/{size:}/{size_unit:}/{size_conv:}/{temp:}/{temp_unit:}'.format(
        url=gcfg.web_service.url,
        port=gcfg.web_service.port,
        run_stat=run_stat,
        user_name=user_name,
        proj_name=proj_name,
        material=material,
        geometry=geometry,
        size=size,
        size_unit=size_unit,
        size_conv=size_conv,
        temp=temp,
        temp_unit=temp_unit
    )
    
    response = session.get(endpoint, verify=False)
    response.raise_for_status()
    
    return json.loads(response.text)


########################################################################
# These functions interact with http services that cause CHANGE        #
########################################################################


def set_running_status(uid, new_status):
    r"""
    Sets the running status of a model.
    :param uid:        the unique id of a model (which is always of the form
                       ``12345678-1234-5678-1234-567812345678``)
    :param new_status: the name of the new status to which the model should
                       be set (a complete set of running statuses may be
                       retrieved with
                       :func:`~m4db.rest.client.running_status.get_all()`
    :returns: a python dictionary containing running status information.
    """
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()

    request_msg = RequestMsgSetRunningStatus()
    request_msg.unique_id = uid
    request_msg.new_status = new_status

    response = session.post(
        'https://{url:}:{port:}/model/set-running-status'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=request_msg.as_json(),
        verify=False
    )
    response.raise_for_status()


def set_quants(data_pod):
    r"""
    Sets the quants of a model.
    :param quants: a dictionary of quantities
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()

    response = session.post(
        'https://{url:}:{port:}/model/set-model-quants'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=data_pod.as_json(),
        verify=False
    )

    response.raise_for_status()

