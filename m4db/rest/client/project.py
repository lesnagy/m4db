r"""
This is a client side module that makes REST-ish requests to m4db's http
http service. The functions in ths module focus around querying and
updating micromagnetic model project data.
"""

import os
import json

# Suppress warnings about insecure requests
import urllib3

import m4db.http_utils

from m4db.global_config import get_global_variables

from m4db.rest.message import RequestMsgSetRunningStatus

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

########################################################################
# These functions only interact with http services that query          #
########################################################################

def get_all_projects():
    r"""
    This gets a list of all user identities on the database
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    endpoint = 'https://{url:}:{port:}/allprojects'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)

def set_new_project(
        name, description):
    r"""
    Sets the identity of a new user .
    :param name: name of the project
    :param description of the project

    :returns: a python dictionary containing new user information.
    """
    gcfg = get_global_variables()

    session = m4db.http_utils.get_retry_session()
    new_user = {"name":name, "description":description}

    response = session.post(
        'https://{url:}:{port:}/newproject'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port
        ),
        data=json.dumps(new_user),
        verify=False
    )
    response.raise_for_status()

def get_project_by_name(
        name):
    r"""
    Sets the identity of a new user .
    :param name: the name of the project to get

    :returns: a python dictionary containing new project information.
    """
    gcfg = get_global_variables()
    session = m4db.http_utils.get_retry_session()
    project_name = name

    endpoint = 'https://{url:}:{port:}/projectbyname/{pname:}'.format(
            url=gcfg.web_service.url,
            port=gcfg.web_service.port,
            pname=project_name
        )
    response = session.get(endpoint, verify=False)

    response.raise_for_status()

    return json.loads(response.text)



if __name__ == "__main__":
    allprojects=get_all_projects()
    print(allprojects)
    namedproject=get_project_by_name("elongations")
    print(namedproject)
    set_new_project( "Hysteresis", "Hysteresis project description")
