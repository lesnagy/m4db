import falcon
import json
import traceback

from m4db.orm import Software

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.global_config import get_global_variables

class GetAllSoftware:

    def on_get(self, req, resp):
        gcfg = get_global_variables()

        dbsession = self.session
        software_query = dbsession.query(Software)
        softwares = software_query.all()

        try:
            if softwares == []:
                resp.status = falcon.HTTP_404
                return
            elif softwares is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    softwares,
                    cls=DefaultModelEncoder
                )
                return

        except:
            # Some sort of error occurred, log
            gcfg.logger("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class NewSoftware:
    def on_post(self, req, resp):
        gcfg = get_global_variables()
        data = req.media
        try:
            new_software = Software()
            if "name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_software.name = data["name"]

            if "version" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_software.version = data["version"]

            if "description" in data.keys():
                new_software.description = data["description"]

            if "url" in data.keys():
                new_software.url = data["url"]

            if "citation" in data.keys():
                new_software.citation = data["citation"]

            self.session.add(new_software)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
