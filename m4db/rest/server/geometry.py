r"""
This module contains a selection of Falcon services
(see documentation for ``m4db.rest.server.app`` for http endpoints) to deal primarily with
querying, adding and updating geometry objects, (i.e. objects of type
:class:`~m4db.orm.Geometry`.
"""

import json
import falcon
import traceback

from sqlalchemy import text

from m4db.global_config import get_global_variables

from m4db.orm import Geometry
from m4db.orm_json_encoder import DefaultGeometryEncoder

###############################################################################
###############################################################################
#     Geometry  GET SERVICES (ones that *DO NOT* change the DB) GO HERE             #
###############################################################################
###############################################################################

class GetGeometryByUniqueId:
    r'''
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Geometry` objects from m4db.
    '''

    def on_get(self, req, resp, unique_id):
        r'''
        Retrieve the information associated with a Neb object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier of the Geometry.
        '''

        gcfg = get_global_variables()

        try:
            geometry = self.session.query(Geometry). \
                filter(Geometry.unique_id == unique_id). \
                one_or_none()

            if geometry is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(
                    geometry,
                    cls=DefaultGeometryEncoder
                )

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return

class GetGeometries:
    r"""
    A class to encapsulate a Falcon REST service to
    retrieve a list of geometries.
    """

    def on_get(self, req, resp):
        """
        Retrieve summary data from the database
        :param req:       the Falcon request object (containing the http post data
                          sent by the user.
        :param resp:      the Falcon response object.
        """
        gcfg = get_global_variables()

        try:
            summary = self.session.execute(
                text(
                    '''
                    select distinct
                        geometry.name,
                        geometry.size,
                        unit.symbol
                    from geometry
                    inner join unit
                        on geometry.size_unit_id = unit.id
                    order by
                        name,
                        size
                    '''
                )
            )

            # construct list of summary data records
            summary_data = []
            for record in summary:
                summary_data.append({
                    'name': record[0],
                    'size': record[1],
                    'unit': record[2]
                })

            gcfg.logger.debug(summary_data)

            if summary_data == []:
                # The models were empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    summary_data
                )
                return
        except:
            # Some sort of error occured, log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
