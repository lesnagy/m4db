r"""
This module contains a selection of Falcon services
(see documentation for ``m4db.rest.server.app`` for http endpoints) to deal primarily with
querying, adding and updating NEB objects, (i.e. objects of type
:class:`~m4db.orm.NEB`.
"""

import os
import falcon
import traceback
import json

from sqlalchemy import and_
from sqlalchemy.orm import aliased

from m4db.orm import Model
from m4db.orm import RunningStatus
from m4db.orm import Project
from m4db.orm import Metadata
from m4db.orm import DBUser
from m4db.orm import NEBCalculationType
from m4db.orm import NEBRunData
from m4db.orm import NEBReportData
from m4db.orm import NEB

from m4db.rest.message import RequestMsgSetRunningStatus

from m4db.global_config import get_global_variables

from m4db.orm_json_encoder import DefaultNebEncoder
import m4db.merrill_script
import m4db.slurm_script
import m4db.util


###############################################################################
###############################################################################
#     NEB GET SERVICES (ones that *DO NOT* change the DB) GO HERE             #
###############################################################################
###############################################################################

class GetNebByUniqueId:
    r'''
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Neb` objects from m4db.
    '''

    def on_get(self, req, resp, unique_id):
        r'''
        Retrieve the information associated with a Neb object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier of the NEB.
        '''

        gcfg = get_global_variables()

        try:
            neb = self.session.query(NEB). \
                filter(NEB.unique_id == unique_id). \
                one_or_none()

            if neb is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(
                    neb,
                    cls=DefaultNebEncoder
                )

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


class GetPathRootByStartEndUniqueIds:
    r"""
    A class to encapsulate a Falcon REST service to retrieve an NEB path
    by using the start/end unique ids of the models that constitute the
    start and end model of the NEB paths. The path is a root, i.e. it has no
    parent.
    """
    def on_get(self, req, resp, unique_id_one, unique_id_two):
        r"""
        Check to see whether an neb with the given start/end unique id exists
        on the system.
        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.
        :param unique_id_one: the unique identifier of a model (either the
                              start/end of a path).
        :param unique_id_two: the unique identifier of another model (either
                              the start/end of a path).
        :return: None
        """
        gcfg = get_global_variables()
        model1 = aliased(Model)
        model2 = aliased(Model)
        try:
            path_one = self.session.query(NEB). \
                join(model1, NEB.start_model_id == model1.id). \
                join(model2, NEB.end_model_id == model2.id). \
                filter(and_(model1.unique_id == unique_id_one, model2.unique_id == unique_id_two)). \
                filter(NEB.parent_neb_id == None). \
                one_or_none()
            if path_one is not None:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"unique_id": path_one.unique_id})
                return
            path_two = self.session.query(NEB). \
                join(model1, NEB.start_model_id == model1.id). \
                join(model2, NEB.end_model_id == model2.id). \
                filter(and_(model1.unique_id == unique_id_two, model2.unique_id == unique_id_one)). \
                filter(NEB.parent_neb_id == None). \
                one_or_none()
            if path_two is not None:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"unique_id": path_two.unique_id})
                return
            resp.status = falcon.HTTP_404
        except:
            resp.status = falcon.HTTP_500
            gcfg.logger(traceback.print_exc())


###############################################################################
###############################################################################
#          MODEL POST SERVICES (ones that change the DB) GO HERE              #
###############################################################################
###############################################################################


###############################################################################
# Falcon service to set model running statuses                                #
###############################################################################

class SetNEBRunningStatus:
    r"""
    A class to encapsulate a Falcon REST service to change the running status
    of a :class:`~m4db.orm.Model` object, i.e. there are not checks as to what
    the existing model's running status is, the running status is just changed.
    """
    def on_post(self, req, resp):
        r"""
        Update a model's `running_status` using http post data. This data
        is in the form of a JSON string and should have the same form as
        outlined in :class:`~m4db.rest.message.model.RunningStatusNew`.

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.
        """
        gcfg = get_global_variables()

        data = req.media

        # Check message
        request_msg = RequestMsgSetRunningStatus()
        try:
            request_msg.from_dict(data)
        except ValueError:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the running status
        running_status = self.session.query(RunningStatus). \
            filter(RunningStatus.name == request_msg.new_status). \
            one_or_none()
        if running_status is None:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the model with unique id
        model = self.session.query(NEB). \
            filter(NEB.unique_id == request_msg.unique_id). \
            one_or_none()

        if model is None:
            # Could not find model, 404 (missing resource)
            resp.status = falcon.HTTP_404
            return

        # Set the running status to new_running_status
        try:
            model.running_status = running_status
            self.session.commit()
        except:
            # Some sort of error - log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return

        # Success
        resp.status = falcon.HTTP_200
        return

###############################################################################
# Falcon service to add an NEB path                                           #
###############################################################################

class AddNotRunNEBPath:
    r"""
    A class to encapsulate a Falcon REST service to add NEB paths.
    """
    def on_post(self, req, resp):
        r"""
        Add an NEB path to the database.

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.

        Note: response returns http errors on following condition

              * 500 - if the start/end point of the path does not exist
                      if the parent path does not exit
                      if the calculation type does not exist
                      if the user name / project does not exist
                      and some other error
        """
        gcfg = get_global_variables()
        data = req.media
        print(data)
        try:
            metadata = self.session.query(Metadata).\
                join(Project, Project.id == Metadata.project_id).\
                join(DBUser, DBUser.id == Metadata.db_user_id).\
                filter(Project.name == data["project_name"]).\
                filter(DBUser.user_name == data["user_name"]).\
                one_or_none()
            if metadata is None:
                # Backend error
                resp.status = falcon.HTTP_500
                gcfg.logger.error(
                    "Could not find user/project: {}/{}".format(
                        data["project_name"],
                        data["user_name"]
                    )
                )
                return
            calculation_type = self.session.query(NEBCalculationType).\
                filter(NEBCalculationType.name == data["calculation_type"]).\
                one_or_none()
            if calculation_type is None:
                # Backend error
                resp.status = falcon.HTTP_500
                gcfg.logger.error(
                    "Could not find calculation type: {}".format(data["calculation_type"])
                )
                return
            running_status = self.session.query(RunningStatus).\
                filter(RunningStatus.name == "not-run").\
                one_or_none()
            if running_status is None:
                # Backend error
                resp.status = falcon.HTTP_500
                gcfg.logger.error(
                    "Could not find running status: unrun"
                )
                return
            if data["start_unique_id"] is not None and data["end_unique_id"] is not None:
                # Retrieve the start model
                start_model = self.session.query(Model).\
                    filter(Model.unique_id == data["start_unique_id"]).\
                    one_or_none()
                if start_model is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find start model with unique id: {}".format(data["start_unique_id"])
                    )
                    return
                # Retrieve the end model
                end_model = self.session.query(Model).\
                    filter(Model.unique_id == data["end_unique_id"]).\
                    one_or_none()
                if end_model is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find end model with unique id: {}".format(data["end_unique_id"])
                    )
                    return
                # Add the new path
                new_neb_path = NEB(
                    spring_constant=data["spring_constant"],
                    curvature_weight=data["curvature_weight"],
                    no_of_points=data["no_of_points"],
                    start_model=start_model,
                    end_model=end_model,
                    neb_calculation_type=calculation_type,
                    neb_run_data=NEBRunData(),
                    neb_report_data=NEBReportData(),
                    running_status=running_status,
                    mdata=metadata
                )
                self.session.add(new_neb_path)
                self.session.commit()
                resp.body = json.dumps({"unique_id": new_neb_path.unique_id})
            elif data["parent_unique_id"] is not None:
                # Retrieve the parent path
                parent_path = self.session.query(NEB).\
                    filter(NEB.unique_id == data["parent_unique_id"]).\
                    one_or_none()
                if parent_path is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find parent path with unique id: {}".format(data["parent_unique_id"])
                    )
                    return
                # Add the new path
                new_neb_path = NEB(
                    spring_constant=data["spring_constant"],
                    curvature_weight=data["curvature_weight"],
                    no_of_points=data["no_of_points"],
                    start_model=parent_path.start_model,
                    end_model=parent_path.end_model,
                    parent_neb=parent_path,
                    neb_calculation_type=calculation_type,
                    neb_run_data=NEBRunData(),
                    neb_report_data=NEBReportData(),
                    running_status=running_status,
                    mdata=metadata
                )
                self.session.add(new_neb_path)
                self.session.commit()
                resp.body = json.dumps({"unique_id": new_neb_path.unique_id})
            # Create the directory for the model and the associated scripts that will be needed to run.
            try:
                neb_uuid_path = m4db.util.mk_uuid_path(
                    new_neb_path.unique_id,
                    root=gcfg.neb_dir
                )
                os.chdir(neb_uuid_path)
                # Create the NEB script
                with open(gcfg.neb_script_merrill, 'w') as fout:
                    fout.write(m4db.merrill_script.merrill_neb_script(new_neb_path))
                # Create the NEB slurm script
                with open(gcfg.slurm_sh, 'w') as fout:
                    fout.write(m4db.slurm_script.slurm_neb_script(new_neb_path))
            except OSError as e:
                gcfg.logger.error(
                   traceback.print_exc()
                )
                resp.status = falcon.HTTP_500
                return
        except Exception:
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


