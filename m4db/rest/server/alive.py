r'''
This is a very simple service that may be used to test whether it is alive.
'''

import falcon

from m4db.rest.message import ResponseMsgBasic, SUCCESS

class Alive:
    r'''
    A REST service that may be used to test whether the server is alive.
    '''

    def on_get(self, req, resp):
        r'''
        The 'get' http request handler.
        '''

        resp.status = falcon.HTTP_200

        response = ResponseMsgBasic()
        response.status = SUCCESS

        resp.body = response.as_json()

        return

