import falcon
import json
import traceback

from m4db.orm import DBUser

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.global_config import get_global_variables

class GetAllUsers:

    def on_get(self, req, resp):
        gcfg = get_global_variables()

        dbsession = self.session
        user_query = dbsession.query(DBUser)
        users = user_query.all()

        try:
            if users == []:
                resp.status = falcon.HTTP_404
                return
            elif users is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    users,
                    cls=DefaultModelEncoder
                )
                return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.format_exc())
            resp.status = falcon.HTTP_500
            return


class NewUser:
    def on_post(self, req, resp):
        gcfg = get_global_variables()
        data = req.media
        try:
            new_user = DBUser()
            if "user_name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.user_name = data["user_name"]

            if  "first_name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.first_name = data["first_name"]

            if "initials" in data.keys():
                new_user.initials = data["initials"]

            if "email" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.email = data["email"]

            if "telephone" in data.keys():
                new_user.telephone = data["telephone"]

            if "surname" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.surname = data["surname"]

            self.session.add(new_user)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.format_exc())
            resp.status = falcon.HTTP_500
            return


