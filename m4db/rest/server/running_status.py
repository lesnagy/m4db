import falcon
import json
import traceback

from m4db.orm import RunningStatus

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.global_config import get_global_variables


class GetAllRunningStatuses:
    r"""
    A class to encapsulate a Falcon REST service to retrieve all running
    statuses (of type :class:`~m4db.orm.RunningStatus`) from m4db.
    """
    def on_get(self, req, resp):
        r"""
        Retrieve all the running statuses held within the m4db database.
        """
        gcfg = get_global_variables()
        
        running_statuses = self.session.query(RunningStatus).all()

        try:
            if running_statuses == []:
                resp.status = falcon.HTTP_404
                return
            elif running_statuses is None:           
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    running_statuses,
                    cls=DefaultModelEncoder
                )
                return
        except:
            # Some sort of error occurred, log
            gcfg.logger("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return