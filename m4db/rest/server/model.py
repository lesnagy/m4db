r'''
This module contains a selection of Falcon services
(see documentation for ``m4db.rest.server.app`` for http endpoints) to deal primarily with
querying, adding and updating model objects, (i.e. objects of type
:class:`~m4db.orm.Model`.
'''

import os
import json
import falcon
import traceback
import mimetypes

from sqlalchemy import text

from m4db.orm import Model
from m4db.orm import RunningStatus
from m4db.orm import Geometry
from m4db.orm import SizeConvention
from m4db.orm import Project
from m4db.orm import Material
from m4db.orm import ModelMaterialAssociation
from m4db.orm import Metadata
from m4db.orm import DBUser

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.rest.message import RequestMsgModelQuants
from m4db.rest.message import RequestMsgSetRunningStatus
from m4db.rest.message import ModelMetadata

from m4db.rest.utilities.model import create_zip

from m4db.global_config import get_global_variables

from m4db.util import convert_to_micron, convert_to_celsius


###############################################################################
###############################################################################
#     MODEL GET SERVICES (ones that *DO NOT* change the DB) GO HERE           #
###############################################################################
###############################################################################


###############################################################################
# Falcon service to retrieve models by a unique ID.                           #
###############################################################################


class GetModelByUniqueId:
    r'''
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Model` objects from m4db.
    '''

    def on_get(self, req, resp, unique_id):
        r'''
        Retrieve the information associated with a Model object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier of the model.
        '''

        gcfg = get_global_variables()

        try:
            model = self.session.query(Model). \
                filter(Model.unique_id == unique_id). \
                one_or_none()

            if model is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(
                    model,
                    cls=DefaultModelEncoder
                )

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


###############################################################################
# Falcon service to retrieve model metadata (project/creation time etc.)      #
###############################################################################


class GetModelMetadata:
    r"""
    A Class to encapsulate a Falcon REST service to retrieve
    :class:`~m4db.orm.Model` metadata from m4db.
    """

    def on_get(self, req, resp, unique_id):
        r"""
        Retrieve the model metadata.
        :param req: the Falcon request object.
        :param resp: the Falcon response object.
        :param unique_id: the unique identifier of the model.

        :return: None
        """
        gcfg = get_global_variables()

        try:
            model = self.session.query(Model). \
                filter(Model.unique_id == unique_id). \
                one_or_none()

            if model is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                model_metadata = ModelMetadata()
                model_metadata.unique_id = model.unique_id
                model_metadata.created = model.created.strftime(gcfg.date_time_format)
                model_metadata.last_modified = model.created.strftime(gcfg.date_time_format)
                model_metadata.db_user = model.mdata.db_user.user_name
                model_metadata.project_name = model.mdata.project.name
                model_metadata.generated_by = "{} (version: {})".format(
                    model.mdata.software.name, model.mdata.software.version
                )

                resp.body = model_metadata.as_json()
        except:
            # Some sort of error occured, log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model zip files                                  #
###############################################################################


def delete_zip_hook(req, resp, resource):
    gcfg = get_global_variables()

    url_split = req.path.split('/')
    zip_file = url_split[2]

    model_tmp_zip = os.path.join(
        gcfg.database.work_root,
        gcfg.model_dir_name,
        zip_file
    )

    try:
        os.remove(model_tmp_zip)
    except FileNotFoundError:
        # If the file could not be found ignore.
        pass


class GetModelZipByUniqueId:
    r'''
    A class to ecapsulate a Falcon REST service that will retrieve
    :class:`~m4db.orm.Model` objects in a zip from from m4db. The resulting
    zip file contains all data including geometry.
    '''

    @falcon.after(delete_zip_hook)
    def on_get(self, req, resp, unique_id_zip):
        r'''
        Retrieve the information associated with a Model object that has
        the unique id given
        '''
        gcfg = get_global_variables()

        unique_id = os.path.splitext(unique_id_zip)[0]

        model = self.session.query(Model). \
            filter(Model.unique_id == unique_id). \
            one_or_none()

        try:
            if model is None:
                resp.status = falcon.HTTP_404
                return
            else:
                zip_file = create_zip(model)

                resp.status = falcon.HTTP_200
                resp.content_type = mimetypes.guess_type(zip_file)[0]
                with open(zip_file, 'rb') as fout:
                    resp.body = fout.read()
                    fout.close()

                return
        except:
            # Some sort of error occured, log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model metadata given a set of parameters         #
###############################################################################


class GetModelsByMaterialGeometrySizeTemperature:
    r'''
    A class to encapsulate a Falcon REST sercvie to retrieve model metadata
    by mgst-tuple (material, geometry, size, temperature)
    '''

    def on_get(self, req, resp, run_stat, user_name, proj_name,
               material, geometry,
               size, size_unit, size_conv,
               temp, temp_unit):
        r'''
        Retrieve model metadata with the specified parameters.

        :param req:       the Falcon request object (containing the http post
                          data sent by the user.
        :param resp:      the Falcon response object.
        :param run_stat:  the m4db running status of the model 'e.g. finished'
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        :param material:  the material name that the models should belong to
        :param geometry:  the geometry name that the models should belong to
        :param size:      the geometry size
        :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
        :param size_conv: the size convention ('esvd' or 'ecvl')
        :param temp:      the temperature
        :param temp_unit: the temperature unit
        '''

        gcfg = get_global_variables()

        # Attempt to retrieve the models:

        try:
            size_um = convert_to_micron(float(size), size_unit)
            temp_C = convert_to_celsius(float(temp), temp_unit)
            size_conv_upper = size_conv.upper()

            print("Size in um: {}".format(size_um))
            print("Tempr in C: {}".format(temp_C))

            models = self.session.query(Model). \
                join(RunningStatus, Model.running_status_id == RunningStatus.id). \
                join(Geometry, Model.geometry_id == Geometry.id). \
                join(SizeConvention, Geometry.size_convention_id == SizeConvention.id). \
                join(ModelMaterialAssociation, Model.id == ModelMaterialAssociation.model_id). \
                join(Material, Material.id == ModelMaterialAssociation.material_id). \
                join(Metadata, Model.mdata_id == Metadata.id). \
                join(Project, Metadata.project_id == Project.id). \
                join(DBUser, Metadata.db_user_id == DBUser.id). \
                filter(RunningStatus.name == run_stat). \
                filter(DBUser.user_name == user_name). \
                filter(Project.name == proj_name). \
                filter(Material.name == material). \
                filter(Geometry.name == geometry). \
                filter(Geometry.size == size_um). \
                filter(SizeConvention.symbol == size_conv_upper). \
                filter(Material.temperature == temp_C). \
                all()
            if models == []:
                # The models were empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    models,
                    cls=DefaultModelEncoder
                )
                return
        except Exception:
            # Some sort of error occured, send a 500 to the user and log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model quants metadata given a set of parameters  #
###############################################################################


class GetModelQuantsByMaterialGeometrySizeTemperature:
    r"""
    A class to encapsulate a Falcon REST sercvie to retrieve model metadata
    by mgst-tuple (material, geometry, size, temperature)
    """

    def on_get(self, req, resp, run_stat, user_name, proj_name,
               material, geometry,
               size, size_unit, size_conv,
               temp, temp_unit):
        r"""
        Retrieve model metadata with the specified parameters.

        :param req:       the Falcon request object (containing the http post
                          data sent by the user.
        :param resp:      the Falcon response object.
        :param run_stat:  the m4db running status of the model 'e.g. finished'
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        :param material:  the material name that the models should belong to
        :param geometry:  the geometry name that the models should belong to
        :param size:      the geometry size
        :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
        :param size_conv: the size convention ('esvd' or 'ecvl')
        :param temp:      the temperature
        :param temp_unit: the temperature unit
        """

        gcfg = get_global_variables()

        # Attempt to retrieve the models:

        try:
            size_um = convert_to_micron(float(size), size_unit)
            temp_C = convert_to_celsius(float(temp), temp_unit)
            size_conv_upper = size_conv.upper()

            gcfg.logger.debug("run_stat: {}".format(run_stat))
            gcfg.logger.debug("user_name: {}".format(user_name))
            gcfg.logger.debug("proj_name: {}".format(proj_name))
            gcfg.logger.debug("material: {}".format(material))
            gcfg.logger.debug("geometry: {}".format(geometry))
            gcfg.logger.debug("size: {}".format(size))
            gcfg.logger.debug("size_unit: {}".format(size_unit))
            gcfg.logger.debug("size_conv: {}".format(size_conv))
            gcfg.logger.debug("temp: {}".format(temp))
            gcfg.logger.debug("temp_unit: {}".format(temp_unit))

            models = self.session.query(Model). \
                join(RunningStatus, Model.running_status_id == RunningStatus.id). \
                join(Geometry, Model.geometry_id == Geometry.id). \
                join(SizeConvention, Geometry.size_convention_id == SizeConvention.id). \
                join(ModelMaterialAssociation, Model.id == ModelMaterialAssociation.model_id). \
                join(Material, Material.id == ModelMaterialAssociation.material_id). \
                join(Metadata, Model.mdata_id == Metadata.id). \
                join(Project, Metadata.project_id == Project.id). \
                join(DBUser, Metadata.db_user_id == DBUser.id). \
                filter(RunningStatus.name == run_stat). \
                filter(DBUser.user_name == user_name). \
                filter(Project.name == proj_name). \
                filter(Material.name == material). \
                filter(Geometry.name == geometry). \
                filter(Geometry.size == size_um). \
                filter(SizeConvention.symbol == size_conv_upper). \
                filter(Material.temperature == temp_C). \
                all()

            if models == []:
                # The models were empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user

                pod_models = []
                for model in models:
                    pod_data = RequestMsgModelQuants()
                    pod_data.unique_id = model.unique_id
                    pod_data.mx_tot = model.mx_tot
                    pod_data.my_tot = model.my_tot
                    pod_data.mz_tot = model.mz_tot
                    pod_data.vx_tot = model.vx_tot
                    pod_data.vy_tot = model.vy_tot
                    pod_data.vz_tot = model.vz_tot
                    pod_data.h_tot = model.h_tot
                    pod_data.adm_tot = model.adm_tot
                    pod_data.e_typical = model.e_typical
                    pod_data.e_anis = model.e_anis
                    pod_data.e_ext = model.e_ext
                    pod_data.e_demag = model.e_demag
                    pod_data.e_exch1 = model.e_exch1
                    pod_data.e_exch2 = model.e_exch2
                    pod_data.e_exch3 = model.e_exch3
                    pod_data.e_exch4 = model.e_exch4
                    pod_data.e_tot = model.e_tot
                    pod_models.append(pod_data.as_dict())

                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    pod_models
                )
                return
        except Exception:
            # Some sort of error occured, inform the user
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model summary                                    #
###############################################################################


class GetModelSummaryByMaterialGeometrySizeTemperature:
    r"""
    A class to encapsulate a Falcon REST service to query the database for
    some useful summary data
    """

    def on_get(self, req, resp, user_name, proj_name):
        r"""
        Retrieve summary data from the database

        :param req:       the Falcon request object (containing the http post data
                          sent by the user.
        :param resp:      the Falcon response object.
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        """

        gcfg = get_global_variables()

        try:
            summary = self.session.execute(
                text(
                    '''
                    select 
                        material_name,
                        geometry_name,
                        geometry_size,
                        material_temperature,
                        count(*)
                    from
                        (
                            select 
                                model.id as model_id,
                                string_agg(material.name, ',') as material_name,
                                geometry.name as geometry_name,
                                concat(geometry.size, unit.symbol) as geometry_size,
                                concat(material.temperature, 'C') as material_temperature
                            from 
                                model 
                                inner join model_material_association as mma 
                                    on mma.model_id = model.id 
                                inner join material 
                                    on mma.material_id = material.id
                                inner join geometry
                                    on model.geometry_id = geometry.id
                                inner join unit
                                    on geometry.size_unit_id = unit.id
                                inner join metadata 
                                    on model.mdata_id = metadata.id
                                inner join db_user
                                    on metadata.db_user_id = db_user.id
                                inner join project
                                    on metadata.project_id = project.id
                            where
                                db_user.user_name like :db_user_name and
                                project.name like :project_name
                            group by 
                                model.id, 
                                geometry_name,
                                geometry_size,
                                material_temperature
                        ) summary
                    group by
                        material_name, geometry_name, geometry_size, material_temperature
                    '''
                ),
                {'db_user_name': user_name, 'project_name': proj_name}
            )

            # construct list of summary data records
            summary_data = []
            for record in summary:
                summary_data.append({
                    'material': record[0],
                    'geometry:': record[1],
                    'size': record[2],
                    'temperature': record[3],
                    'count': record[4]
                })
            if summary_data == []:
                # The models were empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    summary_data
                )
                return
        except:
            # Some sort of error occured, log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
###############################################################################
#          MODEL POST SERVICES (ones that change the DB) GO HERE              #
###############################################################################
###############################################################################


###############################################################################
# Falcon service to set model running statuses                                #
###############################################################################


class SetModelRunningStatus:
    r"""
    A class to encapsulate a Falcon REST service to change the running status
    of a :class:`~m4db.orm.Model` object, i.e. there are not checks as to what
    the existing model's running status is, the running status is just changed.
    """

    def on_post(self, req, resp):
        """
        Update a model's `running_status` using http post data. This data
        is in the form of a JSON string and should have the same form as
        outlined in :class:`~m4db.rest.message.model.RunningStatusNew`.

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.
        """

        gcfg = get_global_variables()
        data = req.media
        print(data)

        # Check message
        request_msg = RequestMsgSetRunningStatus()
        try:
            request_msg.from_dict(data)
        except ValueError:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the running status
        running_status = self.session.query(RunningStatus). \
            filter(RunningStatus.name == request_msg.new_status). \
            one_or_none()
        if running_status is None:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the model with unique id
        model = self.session.query(Model). \
            filter(Model.unique_id == request_msg.unique_id). \
            one_or_none()

        if model is None:
            # Model could not be found (404)
            resp.status = falcon.HTTP_404
            return

        # Set the running status to new_running_status
        try:
            model.running_status = running_status
            self.session.commit()
        except:
            # Some sort of error - log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return

        # Success
        resp.status = falcon.HTTP_200
        return


###############################################################################
# Falcon service to set model quantities                                      #
###############################################################################


class SetModelQuants:
    r"""
    A class to encapsulate a Falcon REST service to set Model quantities.
    """

    def on_post(self, req, resp):
        r"""
        Update model quantities using http post data. This data is in the form
        of a JSON string and should have the same form as outlined in
        :class:`~m4db.rest.message.model.ModelQuants`

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.

        Note: response returns http errors on following condition
              * 400 - the data being sent to the service was invalid/missing values
              * 404 - the model to update was not found
              * 500 - some other error, check logs
        """

        gcfg = get_global_variables()

        data = req.media

        # Attempt to populate pod from dict
        pod_data = RequestMsgModelQuants()
        try:
            pod_data.from_dict(data)
        except ValueError:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Attempt to retrieve model from database
        try:
            model = self.session.query(Model). \
                filter(Model.unique_id == pod_data.unique_id). \
                one_or_none()

            if model is None:
                # Couldn't find model - 404
                resp.status = falcon.HTTP_404
                return
            else:
                model.mx_tot = pod_data.mx_tot
                model.my_tot = pod_data.my_tot
                model.mz_tot = pod_data.mz_tot
                model.vx_tot = pod_data.vx_tot
                model.vy_tot = pod_data.vy_tot
                model.vz_tot = pod_data.vz_tot
                model.h_tot = pod_data.h_tot
                model.adm_tot = pod_data.adm_tot
                model.e_typical = pod_data.e_typical
                model.e_anis = pod_data.e_anis
                model.e_ext = pod_data.e_ext
                model.e_demag = pod_data.e_demag
                model.e_exch1 = pod_data.e_exch1
                model.e_exch2 = pod_data.e_exch2
                model.e_exch3 = pod_data.e_exch3
                model.e_exch4 = pod_data.e_exch4
                model.e_tot = pod_data.e_tot

                self.session.commit()

                resp.status = falcon.HTTP_200
                return
        except:
            # Some sort of error occured, log
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
