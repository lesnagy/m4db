r"""
The application layer.
"""

import falcon

from m4db.database import get_scoped_session

from m4db.rest.middleware import SQLAlchemySessionManager

from m4db.rest.server.model import GetModelByUniqueId
from m4db.rest.server.model import GetModelMetadata
from m4db.rest.server.model import GetModelZipByUniqueId
from m4db.rest.server.model import GetModelsByMaterialGeometrySizeTemperature
from m4db.rest.server.model import GetModelQuantsByMaterialGeometrySizeTemperature
from m4db.rest.server.model import GetModelSummaryByMaterialGeometrySizeTemperature
from m4db.rest.server.model import SetModelQuants
from m4db.rest.server.model import SetModelRunningStatus

from m4db.rest.server.neb import GetPathRootByStartEndUniqueIds
from m4db.rest.server.neb import AddNotRunNEBPath
from m4db.rest.server.neb import SetNEBRunningStatus
from m4db.rest.server.neb import GetNebByUniqueId

from m4db.rest.server.running_status import GetAllRunningStatuses

from m4db.rest.server.geometry import GetGeometries
from m4db.rest.server.geometry import GetGeometryByUniqueId
from m4db.rest.server.alive import Alive

from m4db.rest.server.user import GetAllUsers
from m4db.rest.server.user import NewUser

from m4db.rest.server.software import GetAllSoftware
from m4db.rest.server.software import NewSoftware

from m4db.rest.server.project import GetProjectByName
from m4db.rest.server.project import GetAllProject
from m4db.rest.server.project import NewProject

Session = get_scoped_session()

app = falcon.API(middleware=[
    SQLAlchemySessionManager(Session),
])
app.req_options.auto_parse_form_urlencoded = True

get_model_by_unique_id = GetModelByUniqueId()
get_model_metadata = GetModelMetadata()
get_model_zip_by_unique_id = GetModelZipByUniqueId()
get_models_by_material_geometry_size_temperature = GetModelsByMaterialGeometrySizeTemperature()
get_model_quants_by_material_geometry_size_temperature = GetModelQuantsByMaterialGeometrySizeTemperature()
get_summary_by_material_geometry_size_temperature = GetModelSummaryByMaterialGeometrySizeTemperature()
set_model_quants = SetModelQuants()
set_model_running_status = SetModelRunningStatus()

get_path_root_by_start_end_unique_ids = GetPathRootByStartEndUniqueIds()
add_not_run_neb_path = AddNotRunNEBPath()
set_neb_running_status = SetNEBRunningStatus()
get_neb_by_unique_id = GetNebByUniqueId()

get_all_running_statuses = GetAllRunningStatuses()

get_geometries = GetGeometries()
get_geometry_by_unique_id = GetGeometryByUniqueId()
alive = Alive()

get_all_users = GetAllUsers()
add_new_user = NewUser()

get_all_softwares = GetAllSoftware()
add_new_software = NewSoftware()

get_project_by_name = GetProjectByName()
get_all_projects = GetAllProject()
add_new_project = NewProject()
#######################
# model.py
#######################

app.add_route(
        '/model/{unique_id}',
        get_model_by_unique_id
)

app.add_route(
        '/model-metadata/{unique_id}',
        get_model_metadata
)

app.add_route(
        '/model-zip/{unique_id_zip}',
        get_model_zip_by_unique_id
)

app.add_route(
        '/models/{run_stat}/{user_name}/{proj_name}/{material}/{geometry}/{size}/{size_unit}/{size_conv}/{temp}/{temp_unit}',
        get_models_by_material_geometry_size_temperature
)

app.add_route(
        '/model-quants/{run_stat}/{user_name}/{proj_name}/{material}/{geometry}/{size}/{size_unit}/{size_conv}/{temp}/{temp_unit}',
        get_model_quants_by_material_geometry_size_temperature
)

app.add_route(
        '/model-summary/{user_name}/{proj_name}',
        get_summary_by_material_geometry_size_temperature
)

app.add_route(
        '/model/set-model-quants',
        set_model_quants
)

app.add_route(
        '/model/set-running-status',
        set_model_running_status
)

#######################
# neb.py
#######################

app.add_route(
        '/neb/get-path-root-by-start-end-unique-ids/{unique_id_one}/{unique_id_two}',
        get_path_root_by_start_end_unique_ids
)

app.add_route(
        '/neb/add-not-run',
        add_not_run_neb_path
)

app.add_route(
        '/neb/set-running-status',
        set_neb_running_status
)

app.add_route(
        '/neb/{unique_id}',
        get_neb_by_unique_id
)


#######################
# running_status.py
#######################

app.add_route(
        '/running-status/all',
        get_all_running_statuses
)


#######################
# geometry.py
#######################

app.add_route(
    '/geometry/all',
    get_geometries
)

app.add_route(
        '/geometry/{unique_id}',
        get_geometry_by_unique_id
)

#######################
# alive.py
#######################

app.add_route(
        '/alive',
        alive
)

#######################
# user.py
#######################

app.add_route(
        '/allusers',
        get_all_users
)

app.add_route(
        '/newuser',
        add_new_user
)

#######################
# software.py
#######################

app.add_route(
        '/allsoftwares',
        get_all_softwares
)

app.add_route(
        '/newsoftware',
        add_new_software
)

#######################
# project.py
#######################

app.add_route(
        '/projectbyname/{name}',
        get_project_by_name
)

app.add_route(
        '/allprojects',
        get_all_projects
)

app.add_route(
        '/newproject',
        add_new_project
)
