import falcon
import json
import traceback

from m4db.orm import Project

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.global_config import get_global_variables

###############################################################################
# Falcon service to retrieve models by a project name                           #
###############################################################################


class GetProjectByName:
    r'''
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Project` objects from m4db.
    '''

    def on_get(self, req, resp, name):
        r'''
        Retrieve the information associated with a Model object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param name: the name of the project.
        '''

        gcfg = get_global_variables()

        try:
            project = self.session.query(Project). \
                filter(Project.name == name). \
                one_or_none()

            if project is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(
                    project,
                    cls=DefaultModelEncoder
                )

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


class GetAllProject:

    def on_get(self, req, resp):
        gcfg = get_global_variables()

        dbsession = self.session
        project_query = dbsession.query(Project)
        projects = project_query.all()

        try:
            if projects == []:
                resp.status = falcon.HTTP_404
                return
            elif projects is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    projects,
                    cls=DefaultModelEncoder
                )
                return

        except:
            # Some sort of error occurred, log
            gcfg.logger("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class NewProject:
    def on_post(self, req, resp):
        gcfg = get_global_variables()
        data = req.media
        try:
            new_project = Project()
            if "name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_project.name = data["name"]

            if "description" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_project.description = data["description"]

            self.session.add(new_project)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
