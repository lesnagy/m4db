r"""
This module provides some useful utilites for dealing with
:class: `~m4db.orm.Model` objects such as creating a user readable report
for a model and zipping up a model.
"""

import os
import shutil
import json

from jinja2 import Environment, FileSystemLoader

import m4db.util

#from m4db.orm_json_encoder import *

from m4db.orm_json_encoder import DefaultModelEncoder

from m4db.global_config import get_global_variables


def create_report(model):
    r'''
    This function creates a text report based on the model.
    '''
    gcfg = get_global_variables()

    template_loader = FileSystemLoader(
        searchpath=gcfg.template_report_dir
    )
    template_env = Environment(loader=template_loader)

    template = template_env.get_template(
        gcfg.model_summary_txt_jinja2
    )

    return template.render(model=model)


def create_zip(model):
    r'''
    This function creates a zip file based on the model.
    '''
    gcfg = get_global_variables()

    model_tmp_dir = os.path.join(
        gcfg.database.work_root,
        gcfg.model_dir_name,
        model.unique_id
    )

    model_src_dir = m4db.util.uuid_path(
        model.unique_id,
        root=gcfg.model_dir
    )

    # Check that the directory exists
    if os.path.isdir(model_tmp_dir):
        # If it does exist remove it
        shutil.rmtree(model_tmp_dir)

    # Make the directory in to which we will put our files.
    os.mkdir(model_tmp_dir)

    # Copy over all the model files
    for file_name in os.listdir(model_src_dir):
        src_file_path = os.path.join(model_src_dir, file_name)
        dst_file_path = os.path.join(model_tmp_dir, file_name)
        if os.path.isfile(src_file_path):
            shutil.copyfile(src_file_path, dst_file_path)

    # Make a (human readable) report summary file
    summary_txt = os.path.join(model_tmp_dir, gcfg.model_summary_txt)
    with open(summary_txt, 'w') as fout:
        fout.write(create_report(model))
        fout.write('\n')
        fout.close()

    # Make a JSON report summary file
    summary_json = os.path.join(model_tmp_dir, gcfg.model_summary_json)
    with open(summary_json, 'w') as fout:
        fout.write(
            json.dumps(
                model,
                cls=DefaultModelEncoder
            )
        )
        fout.write('\n')
        fout.close()

    # Zip it up
    zip_file_plain = os.path.join(
        gcfg.database.work_root,
        gcfg.model_dir_name,
        model.unique_id
    )
    zip_file = os.path.join(
        gcfg.database.work_root,
        gcfg.model_dir_name,
        '{}.zip'.format(model.unique_id)
    )
    
    shutil.make_archive(
        zip_file_plain, 'zip', model_tmp_dir
    )

    # Remove the temporary directory
    shutil.rmtree(model_tmp_dir)

    # Return the name of the zip file created
    return zip_file
