import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from m4db.global_config import get_global_variables


def get_retry_session(session=None):
    gcfg = get_global_variables()

    session = session or requests.Session()

    retries = int(gcfg.web_service.retries)
    force_list = [
        int(f) for f in gcfg.web_service.forcelist.split(',')
    ]

    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        status_forcelist=force_list
    )

    adapter = HTTPAdapter(max_retries=retry)

    session.mount('http://', adapter)
    session.mount('https://', adapter)

    return session

