from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

from m4db_thirdparty.ckmeans import optimal_cluster

from m4db.tree import Tree
from m4db.tree import Node


def select_cluster_algorithm(algo_name):
    r"""
    Returns a clustering algorithm based on the input algorithm
    name.

    :param algo_name: the clustering algorithm name to return

    :return: an object that will perform the requested clustering
    """

    if algo_name == 'cluster_algo_1':
        return ClusterAlgorithm1()
    elif algo_name == 'cluster_algo_2':
        return ClusterAlgorithm2()
    elif algo_name == 'cluster_algo_3':
        return ClusterAlgorithm3()
    else:
        raise ValueError(f"Unknown cluster algorithm name '{algo_name}'")


def cluster_factory(*q_params, n_max):
    r"""
    Returns a function to cluster a node's data by 'q_param'.
    :param q_params: the parameters to cluster on
    :param n_max: the maximum number of clusters to look for when choosing the optimum
    :return: a function that acts on a tree node to cluster the node's data items by a single value using a
             one dimensional ckmeans clustering algorithm
    """

    if len(q_params) == 0:
        raise ValueError("No clustering parameters supplied")

    if len(q_params) == 1:
        # There is only one parameter and so we do 1D clustering using ckmeans
        def cluster_by_ckmeans(node: Node):
            data = node.data()

            try:
                param_name = q_params[0]
                #print(param_name)
                if param_name == 'h_tot':
                    ocs = optimal_cluster(data, n_max=n_max, key=lambda cc: round(cc[param_name], 2))
                else:
                    ocs = optimal_cluster(data, n_max=n_max, key=lambda cc: cc[param_name])
            except ValueError:
                # Put everything in to one cluster
                new_child = node.new_child()
                new_child.set_data(data)
                new_child.set_label(",".join(q_params))
                node.set_data([])
                return

            for c in ocs:
                new_child = node.new_child()
                new_child.set_data(c)
                new_child.set_label(",".join(q_params))

            node.set_data([])

        return cluster_by_ckmeans

    if len(q_params) > 1:
        # There is more than one cluster and so use the clustering type
        def cluster_by(node: Node):
            try:
                data = node.data()
                # Filter out the data that we wish to cluster on
                filtered_data = [[di[k] for k in q_params] for di in data]
                # Compute clusters for n = [2, n_max]
                cluster_labels_list = []
                for n_clusters in range(2, n_max+1):
                    if n_clusters > len(filtered_data)-1:
                        break

                    kmeans = KMeans(n_clusters=n_clusters)
                    cluster_labels = kmeans.fit_predict(filtered_data)
                    #print("n_clusters: {}".format(n_clusters))
                    #print("cluster_labels: {}".format(cluster_labels))

                    silhouette_avg = silhouette_score(filtered_data, cluster_labels)
                    #print("silhouette_avg: {}".format(silhouette_avg))
                    cluster_labels_list.append({
                        'silhouette': silhouette_avg,
                        'cluster_labels': cluster_labels
                    })

                # Pick the best clustering based on the maximum silhouette
                optimum_cluster_data = max(cluster_labels_list, key=lambda cll: cll['silhouette'])
                optimum_cluster_labels = optimum_cluster_data['cluster_labels']
                #print("OPTIMUM CLUSTER SILHOUETTE: {}".format(optimum_cluster_data['silhouette']))
                #print("OPTIMUM CLUSTER LABELS: {}".format(optimum_cluster_data['cluster_labels']))

                # Create a child for each cluster and add quants
                n_clusters = len(set(optimum_cluster_labels))
                #print("Number of clusters in optimum clustering: {}".format(n_clusters))

                children_cluster_data = [[] for i in range(n_clusters)]
                for cluster_label, item in zip(optimum_cluster_labels, data):
                    children_cluster_data[cluster_label].append(item)
                for quants in children_cluster_data:
                    new_child = node.new_child()
                    new_child.set_data(quants)
                    new_child.set_label(",".join(q_params))

                node.set_data([])
            except ValueError:
                # Put everything in to one cluster
                new_child = node.new_child()
                new_child.set_data(data)
                new_child.set_label(",".join(q_params))
                node.set_data([])
        return cluster_by


###############################################################################
# Clustering algorithms are defined here                                      #
###############################################################################


class ClusterAlgorithm:
    r"""
    This is the root clustering algorithm, it may be used to cluster
    values in a tree.
    """

    def __init__(self, *q_params):
        self._pipeline = []
        for q_param in q_params:
            split_q_param = [qp.strip() for qp in q_param.split(',')]
            #print(split_q_param)
            self._pipeline.append(
                cluster_factory(*split_q_param, n_max=20)
            )

    def __call__(self, quants):
        # Create a tree
        tree = Tree()

        # Add data to the root
        tree.root().set_data(quants)

        # Run the clustering functions on each tree level
        for pipeline_fun in self._pipeline:
            tree.depth_first_left_right(leaf_fun=pipeline_fun)

        # # TODO: remove below
        # def print_node(node: Node):
        #     indent = "\t"*node.level()
        #     print("{} id:{}, level:{} (has {} data items)".format(indent, node.id(), node.level(), len(node.data())))
        #
        # def print_leaf_node(node: Node):
        #     indent = "\t"*node.level()
        #     print("{} id:{}, level:{} (has {} data items)".format(indent, node.id(), node.level(), len(node.data())))
        #
        # new_tree = tree_from_json(tree.get_json())
        # new_tree.depth_first_left_right(node_fun=print_node, leaf_fun=print_leaf_node)
        #
        # print("")
        # tree.depth_first_left_right(node_fun=print_node, leaf_fun=print_leaf_node)
        # # TODO: remove above

        return tree


class ClusterAlgorithm1(ClusterAlgorithm):
    r"""
    This clustering algorithm will construct a cluster of three levels:
    1) by helicity, 2) by energy 3) by direction.
    """
    def __init__(self):
        super().__init__('h_tot', 'mx_tot,my_tot,mz_tot')


class ClusterAlgorithm2(ClusterAlgorithm):
    r"""
    This clustering algorithm will construct a cluster of three levels:
    1) by helicity, 2) by energy 3) by direction.
    """
    def __init__(self):
        super().__init__('h_tot', 'e_tot', 'mx_tot,my_tot,mz_tot')


class ClusterAlgorithm3(ClusterAlgorithm):
    r"""
    This clustering algorithm will construct a cluster of three levels:
    1) by helicity, 2) by energy 3) by direction.
    """
    def __init__(self):
        super().__init__('e_tot', 'mx_tot,my_tot,mz_tot', 'h_tot')
