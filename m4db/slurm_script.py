r"""
This is a set of routines that will produce slurm scripts.
"""


import os

from jinja2 import Environment, FileSystemLoader

import m4db.util

from m4db.orm import Model
from m4db.orm import NEB

from m4db.global_config import get_global_variables


def slurm_model_script(model: Model):
    r"""
    This function generates the 'standard' slurm model runner script.
    """

    gcfg = get_global_variables()

    template_dir = os.path.join(gcfg.home, 'base_data', 'templates')
    template_loader = FileSystemLoader(searchpath=template_dir)
    template_env = Environment(loader=template_loader)

    template = template_env.get_template(
        gcfg.slurm_model_jinja2
    )

    uuid_dir = m4db.util.uuid_path(model.unique_id, gcfg.model_dir)

    tslurm = {}

    tslurm['unique_id'] = model.unique_id
    tslurm['N'] = 1
    tslurm['n'] = 1
    tslurm['c'] = 1
    tslurm['time'] = '99:99:99'
    tslurm['merrill_script'] = gcfg.model_script_merrill
    tslurm['model_stdout_txt'] = gcfg.model_stdout_txt
    tslurm['model_stderr_txt'] = gcfg.model_stderr_txt
    tslurm['working_directory'] = uuid_dir
    tslurm['environment'] = os.path.join(gcfg.m4db_root, "environ", gcfg.m4db_version)

    return template.render(sdata=tslurm)


def slurm_neb_script(neb: NEB):
    r"""
    This function generates the 'standard' slurm NEB runner script.
    :param neb: the neb database object associated with the path that the SLURM runner script will execute
    :return: the SLURM runner script text
    """
    gcfg = get_global_variables()
    template_dir = os.path.join(gcfg.home, 'base_data', 'templates')
    template_loader = FileSystemLoader(searchpath=template_dir)
    template_env = Environment(loader=template_loader)
    template = template_env.get_template(
        gcfg.slurm_neb_jinja2
    )
    uuid_dir = m4db.util.uuid_path(neb.unique_id, gcfg.neb_dir)
    slurm_data = {
        'unique_id': neb.unique_id,
        'N': 1,
        'n': 1,
        'c': 1,
        'time': '99:99:99',
        'merrill_script': gcfg.neb_script_merrill,
        'neb_stdout_txt': gcfg.neb_stdout_txt,
        'neb_stderr_txt': gcfg.neb_stderr_txt,
        'working_directory': uuid_dir,
        'environment': os.path.join(gcfg.m4db_root, "environ", gcfg.m4db_version)
    }
    return template.render(sdata=slurm_data)
