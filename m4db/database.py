"""
This module contains a collection of useful database-centric utility functions.
"""


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

from m4db import orm

from m4db.global_config import get_global_variables


def get_session(create=False, echo=False):
    r"""
    Retrieve a database session object for the given database.

    Args:
        create: if true the database will be created and then the session
                will be retrieved.

        echo:   if true sql statements sent to the SQL alchemy ORM will will
                be echoed to standard output.

    Throws:
        None

    Returns:
        an SQL-alchemy session that may be used to query the database.
    """
    gcfg = get_global_variables()

    if not hasattr(get_session, 'engine'):

        db_url = gcfg.database.url_template.format(
            user=gcfg.database.user,
            host=gcfg.database.host,
            db=gcfg.database.db
        )

        get_session.engine = create_engine(db_url, echo=echo)
        get_session.engine.connect()

        get_session.session_class = sessionmaker(
            bind=get_session.engine
        )

    if create:
        if hasattr(orm.Base, 'metadata'):
            metadata = getattr(orm.Base, 'metadata')
            metadata.create_all(get_session.engine)
        else:
            raise AssertionError("Fatal, orm.Base has no attribute 'metadata'")
        # orm.Base.metadata.create_all(get_session.engine)

    session = get_session.session_class()

    return session


def get_scoped_session():
    gcfg = get_global_variables()

    if not hasattr(get_scoped_session, 'engine'):
        db_url = gcfg.database.url_template.format(
            user=gcfg.database.user,
            host=gcfg.database.host,
            db=gcfg.database.db
        )

        get_scoped_session.engine = create_engine(db_url)

        get_scoped_session.session_class = sessionmaker(
            bind=get_scoped_session.engine
        )

    return scoped_session(get_scoped_session.session_class)



