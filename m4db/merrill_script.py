r"""
This is a set of routines that will produce merrill scripts.
"""


import os

from jinja2 import Environment, FileSystemLoader

from m4db.orm import RandomField
from m4db.orm import UniformField
from m4db.orm import ModelField
from m4db.orm import Model
from m4db.orm import NEB

from m4db.global_config import get_global_variables

from m4db.util import uuid_path


def merrill_model_script(model: Model):
    r"""
    This function generates the 'standard' merrill model script, it executes a
    model with a given geometry and material (and possibly a given ambient field).

    Arguments:
        model : and m4db.orm.Model object (see orm.py)

    Output:
        a merrill script string that may be saved to a file for execution.
    """
    gcfg = get_global_variables()

    template_dir = os.path.join(gcfg.home, 'base_data', 'templates')
    template_loader = FileSystemLoader(searchpath=template_dir)
    template_env = Environment(loader=template_loader)

    template = template_env.get_template(gcfg.merrill_model_jinja2)

    tmodel = {}

    # Set the model's mesh
    abs_mesh_file = os.path.join(
        gcfg.database.file_root,
        gcfg.geometry_dir_name,
        uuid_path(model.geometry.unique_id),
        gcfg.geometry_pat
    )
    tmodel['mesh_file'] = abs_mesh_file

    # Set some of the model's execution parameters
    tmodel['max_evals'] = model.max_energy_evaluations
    tmodel['minimizer'] = 'ConjugateGradient'
    tmodel['exchange_calculator'] = 1

    # Set the initial field data
    tmodel['initial_field'] = {}
    if type(model.start_magnetization) is RandomField:
        tmodel['initial_field']['type'] = 'random'
    elif type(model.start_magnetization) is UniformField:
        start_magnetization: UniformField = model.start_magnetization
        tmodel['initial_field']['type'] = 'uniform'
        tmodel['initial_field']['x'] = start_magnetization.dir_x
        tmodel['initial_field']['y'] = start_magnetization.dir_y
        tmodel['initial_field']['z'] = start_magnetization.dir_z
    elif type(model.start_magnetization) is ModelField:
        tmodel['initial_field']['type'] = 'model'
        tmodel['initial_field']['dat_file'] = gcfg.magnetization_dat

    # Set the material
    tmodel['material'] = {}
    tmodel['material']['name'] = model.materials[0].material.name
    tmodel['material']['temperature'] = model.materials[0].material.temperature
    tmodel['material']['ms'] = model.materials[0].material.ms
    tmodel['material']['k1'] = model.materials[0].material.k1
    tmodel['material']['aex'] = model.materials[0].material.aex

    # Set the model ambient field
    tmodel['ambient_field'] = {}
    if model.external_field is None:
        # There is no external field
        tmodel['ambient_field']['strength'] = 0
        tmodel['ambient_field']['unit'] = 'mT'
        tmodel['ambient_field']['x'] = -1.0
        tmodel['ambient_field']['y'] = -1.0
        tmodel['ambient_field']['z'] = -1.0
    else:
        # There is an external field, so use its values
        external_field: UniformField = model.external_field
        tmodel['ambient_field']['strength'] = external_field.magnitude
        tmodel['ambient_field']['unit'] = external_field.unit
        tmodel['ambient_field']['x'] = external_field.dir_x
        tmodel['ambient_field']['y'] = external_field.dir_y
        tmodel['ambient_field']['z'] = external_field.dir_z

    # Set the log file
    tmodel['energy_log_file'] = 'energy'

    # Set the output
    tmodel['output'] = gcfg.magnetization

    return template.render(model=tmodel)


def merrill_neb_script(neb: NEB):
    r"""
    This function generates a MERRILL 'NEB' script that will be used to compute an NEB path
    :param neb:
    :return:
    """
    gcfg = get_global_variables()

    template_dir = os.path.join(gcfg.home, 'base_data', 'templates')
    template_loader = FileSystemLoader(searchpath=template_dir)
    template_env = Environment(loader=template_loader)

    if neb.parent_neb is None:
        # This NEB path has no parent and so we'll use the 'root' MERRILL template
        template = template_env.get_template(gcfg.merrill_neb_root_path_jinja2)
        geometry_file = os.path.join(
            uuid_path(neb.start_model.geometry.unique_id, root=gcfg.geometry_dir),
            gcfg.geometry_pat
        )
        start_magnetization_file = os.path.join(
            uuid_path(neb.start_model.unique_id, root=gcfg.model_dir),
            gcfg.magnetization_dat
        )
        end_magnetization_file = os.path.join(
            uuid_path(neb.end_model.unique_id, root=gcfg.model_dir),
            gcfg.magnetization_dat
        )
        # TODO: Only using the first material!
        template_data = {
            'mesh_file': geometry_file,
            'max_energy_evaluations': neb.max_energy_evaluations,
            'max_path_evaluations': neb.max_path_evaluations,
            'material': {
                'name': neb.start_model.materials[0].material.name,
                'temperature': neb.start_model.materials[0].material.temperature,
                'ms': neb.start_model.materials[0].material.ms,
                'k1': neb.start_model.materials[0].material.k1,
                'aex': neb.start_model.materials[0].material.aex,
            },
            'external_field': {
                'strength': neb.external_field.magnitude if neb.external_field is not None else 0.0,
                'unit':  neb.external_field.unit if neb.external_field is not None else 'mT',
                'x': neb.external_field.dir_x if neb.external_field is not None else -1.0,
                'y': neb.external_field.dir_y if neb.external_field is not None else -1.0,
                'z': neb.external_field.dir_z if neb.external_field is not None else -1.0,
            },
            'start_magnetization': start_magnetization_file,
            'end_magnetization': end_magnetization_file,
            'energy_log_file': 'energy',
            'neb_path_points': neb.no_of_points,
            'minimizer': 'ConjugateGradient',
            'exchange_calculator': 1,
            'neb_file_name': gcfg.neb_tec
        }
        return template.render(data=template_data)
    else:
        # This NEB path has a parent and so we'll use the 'child' MERRILL template
        template = template_env.get_template(gcfg.merrill_neb_child_path_jinja2)
        initial_path = os.path.join(
            uuid_path(neb.parent_neb.unique_id, root=gcfg.neb_dir),
            gcfg.neb_tec
        )
        # TODO: Only using the first material!
        template_data = {
            'neb_initial_path_name': initial_path,
            'max_energy_evaluations': neb.max_energy_evaluations,
            'max_path_evaluations': neb.max_path_evaluations,
            'material': {
                'name': neb.start_model.materials[0].material.name,
                'temperature': neb.start_model.materials[0].material.temperature,
                'ms': neb.start_model.materials[0].material.ms,
                'k1': neb.start_model.materials[0].material.k1,
                'aex': neb.start_model.materials[0].material.aex,
            },
            'external_field': {
                'strength': neb.external_field.magnitude if neb.external_field is not None else 0.0,
                'unit':  neb.external_field.unit if neb.external_field is not None else 'mT',
                'x': neb.external_field.dir_x if neb.external_field is not None else -1.0,
                'y': neb.external_field.dir_y if neb.external_field is not None else -1.0,
                'z': neb.external_field.dir_z if neb.external_field is not None else -1.0,
            },
            'energy_log_file': 'energy',
            'minimizer': 'ConjugateGradient',
            'exchange_calculator': 1,
            'neb_file_name': gcfg.neb_tec
        }
        return template.render(data=template_data)


def merrill_energy_script(model):
    r"""
    This function generates a merrill 'energies' script that will be used to extract
    the different energies of a particular structure, it executes a
    model with a given geometry and material (and possibly a given ambient field).

    Arguments:
        model : and m4db.orm.Model object (see orm.py)

    Output:
        a merrill script string that may be saved to a file for execution.
    """

    gcfg = get_global_variables()

    template_dir = os.path.join(gcfg.home, 'base_data', 'templates')
    template_loader = FileSystemLoader(searchpath=template_dir)
    template_env = Environment(loader=template_loader)

    template = template_env.get_template(gcfg.merrill_energies.jinja2)

    tmodel = {}

    # Set the model's mesh
    abs_mesh_file = os.path.join(
        gcfg.database.file_root,
        gcfg.geometry_dir_name,
        uuid_path(model.geometry.unique_id),
        gcfg.geometry_pat
    )

    tmodel['mesh_file'] = abs_mesh_file

    # Set some of the model's execution parameters
    tmodel['max_evals'] = model.max_energy_evaluations
    tmodel['minimizer'] = 'ConjugateGradient'
    tmodel['exchange_calculator'] = 1

    # Set the material
    tmodel['material'] = {}
    tmodel['material']['name'] = model.materials[0].material.name
    tmodel['material']['temperature'] = model.materials[0].material.temperature
    tmodel['material']['ms'] = model.materials[0].material.ms
    tmodel['material']['k1'] = model.materials[0].material.k1
    tmodel['material']['aex'] = model.materials[0].material.aex

    # Set he model ambient field
    tmodel['ambient_field'] = {}
    if model.external_field is None:
        # There is no external field
        tmodel['ambient_field']['strength'] = 0
        tmodel['ambient_field']['unit'] = 'mT'
        tmodel['ambient_field']['x'] = -1.0
        tmodel['ambient_field']['y'] = -1.0
        tmodel['ambient_field']['z'] = -1.0
    else:
        # There is an external field, so use its values
        theta = model.external_field.theta
        phi = model.external_field.phi

        x,y,z = UniformField.spherical_to_cartesian_direction(theta, phi)

        tmodel['ambient_field']['strength'] = model.external_field.magnitude
        tmodel['ambient_field']['unit'] = model.external_field.unit
        tmodel['ambient_field']['x'] = x
        tmodel['ambient_field']['y'] = y
        tmodel['ambient_field']['z'] = z

    # Set the magnetization
    tmodel['field'] = {}
    tmodel['field']['dat_file'] = gcfg.magnetization_dat

    return template.render(model=tmodel)

