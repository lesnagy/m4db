r"""
A class that may be used to visualize field data using VTK.
"""
import vtk

from m4db.vtk_util import pathIdNameM
from m4db.vtk_util import pathIdNameH
from m4db.vtk_util import tec_to_unstructured_grid_visualize


class VisualizerVTK:
    def __init__(self, tec_file):
        self.ug_data = tec_to_unstructured_grid_visualize(tec_file)

        # Arrows
        self.arrowSource = vtk.vtkArrowSource()
        self.arrowSource.SetShaftResolution(30)
        self.arrowSource.SetTipResolution(50)

        self.transform = vtk.vtkTransform()
        self.transform.Translate([-0.5, 0.0, 0.0])

        self.transPolyData = vtk.vtkTransformPolyDataFilter()
        self.transPolyData.SetTransform(self.transform)
        self.transPolyData.SetInputConnection(self.arrowSource.GetOutputPort())

        # Vectors (glyphs)
        self.glyph = vtk.vtkGlyph3D()
        self.glyph.SetSourceConnection(self.transPolyData.GetOutputPort())
        self.glyph.SetInputData(self.ug_data.ug)
        self.glyph.ScalingOn()
        self.glyph.SetScaleModeToScaleByVector()
        self.glyph.SetVectorModeToUseVector()
        self.glyph.SetColorModeToColorByScalar()
        self.glyph.OrientOn()
        self.glyph.SetScaleFactor(0.020)
        self.glyph.Update()

        # Colour lookup table
        self.nlut = 1000
        self.lut = vtk.vtkLookupTable()
        self.lut.SetNumberOfTableValues(self.nlut)
        self.ctf = vtk.vtkColorTransferFunction()

        if self.ug_data.hel_mids[0] < 0.0:
            self.ctf.AddRGBSegment(self.ug_data.hel_mins[0], 1.0, 0.0, 0.0,
                                   self.ug_data.hel_maxs[0], 1.0, 0.8, 0.8)
        else:
            self.ctf.AddRGBSegment(self.ug_data.hel_mins[0], 0.8, 0.8, 1.0,
                                   self.ug_data.hel_maxs[0], 0.0, 0.0, 1.0)

        dHel = abs(self.ug_data.hel_maxs[0] - self.ug_data.hel_mins[0]) / float(self.nlut)
        for i in range(self.nlut):
            sv = self.ug_data.hel_mins[0] + float(i)*dHel
            cv = self.ctf.GetColor(sv)
            self.lut.SetTableValue(i, cv[0], cv[1], cv[2])

        # Field mapper and actor
        self.fieldMapper = vtk.vtkPolyDataMapper()
        self.fieldMapper.SetInputConnection(self.glyph.GetOutputPort())
        self.fieldMapper.ScalarVisibilityOn()
        self.fieldMapper.SetLookupTable(self.lut)
        self.fieldMapper.SetScalarRange(self.ug_data.hel_mins[0], self.ug_data.hel_maxs[0])
        self.fieldMapper.Update()

        self.fieldActor = vtk.vtkActor()
        self.fieldActor.SetMapper(self.fieldMapper)

        self.ug_data.ug.GetPointData().SetActiveVectors(pathIdNameM(0))
        self.ug_data.ug.GetPointData().SetActiveScalars(pathIdNameH(0))
        self.fieldMapper.Update()




