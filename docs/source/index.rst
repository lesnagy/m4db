.. m4db documentation master file, created by
   sphinx-quickstart on Thu Jun 27 11:18:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to m4db's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   bin
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
