m4db executable utilities
=========================

The `m4db` package provides a number of command line executable tools which are
listed as follows.

m4db_add\_geometry
------------------

The ``m4db\_add\_geometry`` utility can be used to add a geometry mesh to the
database.

.. argparse::
   :filename: ../bin/m4db_add_geometry
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_add_geometry

m4db\_cluster\_run
------------------

The ``m4db_cluster_run`` utiliity can be used to discover pairwise models for
nudged elastic band (NEB) minimization.

.. argparse::
   :filename: ../bin/m4db_cluster_run
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_cluster_run

m4db\_import\_geometries
------------------------

The ``m4db_import_geometries`` utility can be used to import a bunch of 
geometries from the legacy data directory structure.

.. argparse::
   :filename: ../bin/m4db_import_geometries
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_import_geometries

m4db\_import\_models
--------------------

The ``m4db_import_models`` utility can be used to import a bunch of 
models from the legacy data directory structure.

.. argparse::
   :filename: ../bin/m4db_import_models
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_import_models

m4db\_model\_add\_unrun
-----------------------

The ``m4db_model_add_unrun`` utility adds one or more new unrun models to the
database, these may then be scheduled to run using ``m4db_schedule_unrun``.

.. argparse::
   :filename: ../bin/m4db_model_add_unrun
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_model_add_unrun

m4db\_model\_postprocess
------------------------

The ``m4db_model_postprocess`` utility will compute additional quantities and
extra output files for a particular model.

.. argparse::
   :filename: ../bin/m4db_model_postprocess
   :func: get_cmd_line_parser
   :prog: ../bin/m4db_model_postprocess


