#!/usr/bin/env python

r"""
Query the database for a list of model quantities.
"""
from argparse import ArgumentParser

from jinja2 import Environment, FileSystemLoader

from m4db.global_config import get_global_variables

from m4db.rest.client.model import get_model_quants_by_material_geometry_size_temperature


def get_cmd_line_parser():
    
    parser = ArgumentParser()
    
    parser.add_argument('username',
        help='the user that owns the models'
    )
    
    parser.add_argument('project',
        help='the project name to which the models belong'
    )

    parser.add_argument('material',
        help='the name of the material'
    )
    parser.add_argument('geometry',
        help='the name of a geometry'
    )
    parser.add_argument('size', type=float,
        help='the size of the geometry'
    )
    parser.add_argument('size_unit', default='um',
        help='the size unit',
        choices=['m', 'cm', 'mm', 'um', 'nm', 'fm', 'pm', 'am']
    )
    parser.add_argument('temperature', type=float,
        help='the temperature of the material'
    )
    parser.add_argument('temperature_unit', default='C',
        help='the temperature unit', 
        choices=['C', 'K', 'F']
    )
    
    parser.add_argument('--run_status',
        help='the mode running statuses', required=False,
        default='finished',
        choices=['not-run', 're-run', 'running', 'finished', 'crashed', 'scheduled']
    )
    
    parser.add_argument('--size_convention',
        help='the size convention to use', required=False,
        default='esvd',
        choices=['esvd', 'ecvl']
    )

    return parser



def display_model(quants_list):
    r'''
    Displays the model using the summary information template.
    '''
    
    gcfg = get_global_variables()

    template_loader = FileSystemLoader(
        searchpath=gcfg.template_report_dir
    )
    template_env = Environment(loader=template_loader)

    template = template_env.get_template(
        gcfg.models_mgst_quants_stdout_jinja2
    )

    print(template.render(ql=quants_list))



def main():
    parser = get_cmd_line_parser()

    args = parser.parse_args()

    model = get_model_quants_by_material_geometry_size_temperature(
        args.run_status,
        args.username,
        args.project,
        args.material,
        args.geometry,
        args.size,
        args.size_unit,
        args.size_convention,
        args.temperature,
        args.temperature_unit
    )
    # print(model)

    display_model(model) 
        
            
            
if __name__ == '__main__':
    main()