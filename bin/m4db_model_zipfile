#!/usr/bin/env python

r"""
Query the database for a model.
"""

import requests

from argparse import ArgumentParser

from m4db.rest.client.model import get_model_zip


def get_cmd_line_parser():
    parser = ArgumentParser()

    parser.add_argument('unique_id',
                        help='The unique id of a model for which we wish to retrieve zip data')

    parser.add_argument('-v', action='store_true',
                        help='Display verbose information (could be useful for debugging)')

    return parser


def main():
    parser = get_cmd_line_parser()

    args = parser.parse_args()

    try:
        get_model_zip(args.unique_id)
    except requests.exceptions.HTTPError as http_err:
        if http_err.response.status_code == 404:
            print("The model '{}' does not exist".format(args.unique_id))
        else:
            print("HTTP error code: {} while attempting to download zip".format(
                http_err.response.status_code
            ))
    else:
        print("The file {}.zip downloaded!".format(args.unique_id))


if __name__ == '__main__':
    main()